package ba.etf.rma21.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity
data class Kviz(

        @PrimaryKey @SerializedName("id") var id: Int,
        @ColumnInfo(name = "naziv") @SerializedName("naziv") var naziv: String,
        @ColumnInfo(name = "datumPocetka") @SerializedName("datumPocetak") var datumPocetka: Date,
        @ColumnInfo(name = "datumKraj") @SerializedName("datumKraj") var datumKraj: Date?,
        @ColumnInfo(name = "trajanje") @SerializedName("trajanje") var trajanje: Int,
        @ColumnInfo(name = "nazivPredmeta") var nazivPredmeta: String,
        @ColumnInfo(name = "datumRada") var datumRada: Date?,
        var nazivGrupe: String,
        @ColumnInfo(name = "osvojeniBodovi") var osvojeniBodovi: Float?,
        @ColumnInfo(name = "predan") var predan: Boolean
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Kviz

        if (naziv != other.naziv) return false
        if (nazivPredmeta != other.nazivPredmeta) return false
        if (datumPocetka != other.datumPocetka) return false
        if (datumKraj != other.datumKraj) return false
        if (trajanje != other.trajanje) return false
        if (nazivGrupe != other.nazivGrupe) return false

        return true
    }


    override fun hashCode(): Int {
        var result = naziv.hashCode()
        result = 31 * result + nazivPredmeta.hashCode()
        result = 31 * result + datumPocetka.hashCode()
        result = 31 * result + datumKraj.hashCode()
        result = 31 * result + trajanje
        result = 31 * result + nazivGrupe.hashCode()
        return result
    }
}


