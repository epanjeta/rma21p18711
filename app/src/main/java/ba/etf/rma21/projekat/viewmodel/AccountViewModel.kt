package ba.etf.rma21.projekat.viewmodel

import ba.etf.rma21.projekat.data.repositories.AccountRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class AccountViewModel {
    val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun postaviHash(payload: String?, onSuccess: () -> Unit,
                    onError: () -> Unit){
        scope.launch {
            AccountRepository.postaviHash(payload)
        }
    }
}