package ba.etf.rma21.projekat.data.models

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity
data class KvizTaken(
        @PrimaryKey @SerializedName("id") var id: Int,
        @ColumnInfo(name = "student") @SerializedName("student") var student: String?,
        @ColumnInfo(name = "osvojeniBodovi") @SerializedName("osvojeniBodovi") var osvojeniBodovi: Int,
        @ColumnInfo(name = "datumRada") @SerializedName("datumRada") var datumRada: Date,
        @ColumnInfo(name = "KvizId") @SerializedName("KvizId") var KvizId: Int,
        var tacniOdgovori: Int


) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as KvizTaken

        if (id != other.id) return false
        if (osvojeniBodovi != other.osvojeniBodovi) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + osvojeniBodovi
        return result
    }
}

