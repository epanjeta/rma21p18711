package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import android.util.Log
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Odgovor
import ba.etf.rma21.projekat.data.models.OdgovorBody
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*

class OdgovorRepository {

    companion object {

        private lateinit var context: Context
        var uradjenNoviKviz = false
        fun setContext(_context:Context){
            context=_context
        }
        fun getContext() : Context {return context}

    suspend fun postaviOdgovorKviz(idKvizTaken:Int,idPitanje:Int,odgovor:Int):Int {
        return withContext(Dispatchers.IO) {

            var nijeOdgovoreno = postaviOdgovorUTabelu(idKvizTaken, idPitanje, odgovor)
            var db = AppDatabase.getInstance(context)
            if(!nijeOdgovoreno) {
                var hash = AccountRepository.getHash()
                var pokusaji = ApiAdapter.retrofit.getAttempts(hash)
                for (pokusaj in pokusaji) {
                    if (pokusaj.id == idKvizTaken) {
                        var pitanja = db.pitanjeDao().getPitanjaZaKviz(pokusaj.KvizId)
                        var odgovori = db.odgovorDao().getAll()
                        var bodovi: Int = 0
                        var brojTacnih: Int = 0
                        for (odgovor in odgovori) {
                            for (pitanje in pitanja) {
                                if (odgovor.pitanjeId == pitanje.id) {
                                    if (odgovor.odgovoreno == pitanje.tacan) brojTacnih++
                                }
                            }
                        }
                        bodovi = ((brojTacnih.toDouble() / pitanja.size.toDouble()) * 100.toDouble()).toInt()
                        return@withContext bodovi
                    }
                }
            }
            else{
                var hash = AccountRepository.getHash()
                var pokusaji = ApiAdapter.retrofit.getAttempts(hash)
                for (pokusaj in pokusaji) {
                    if (pokusaj.id == idKvizTaken) {
                        var pitanja = db.pitanjeDao().getPitanjaZaKviz(pokusaj.KvizId)
                        var odgovori = getOdgovoriKviz(pokusaj.KvizId)
                        var bodovi: Int = 0
                        var brojTacnih: Int = 0
                        for (odgovor in odgovori) {
                            for (pitanje in pitanja) {
                                if (odgovor.pitanjeId == pitanje.id) {
                                    if (odgovor.odgovoreno == pitanje.tacan) brojTacnih++
                                }
                            }
                        }
                        for (pitanje in pitanja) {
                            if (pitanje.id == idPitanje) {
                                if (pitanje.tacan == odgovor) {
                                    brojTacnih++
                                    bodovi = ((brojTacnih.toDouble() / pitanja.size.toDouble()) * 100.toDouble()).toInt()
                                    return@withContext bodovi
                                } else {
                                    bodovi = ((brojTacnih.toDouble() / pitanja.size.toDouble()) * 100.toDouble()).toInt()
                                    return@withContext bodovi
                                }
                            }
                        }
                    }
                }
            }

//            var hash = AccountRepository.getHash()
//            var pokusaji = ApiAdapter.retrofit.getAttempts(hash)
//            for (pokusaj in pokusaji) {
//                if (pokusaj.id == idKvizTaken) {
//                    var pitanja = ApiAdapter.retrofit.getQuestionsForKviz(pokusaj.KvizId)
//                    var odgovori = getOdgovoriKviz(pokusaj.KvizId)
//                    var bodovi:Int = 0
//                    var brojTacnih:Int = 0
//                    for(odgovor in odgovori){
//                        for(pitanje in pitanja){
//                            if(odgovor.pitanjeId == pitanje.id){
//                                if(odgovor.odgovoreno == pitanje.tacan) brojTacnih++
//                            }
//                        }
//                    }
//                    for (pitanje in pitanja) {
//                        if (pitanje.id == idPitanje) {
//                            if(pitanje.tacan == odgovor){
//                                brojTacnih++
//                                bodovi = ((brojTacnih.toDouble() / pitanja.size.toDouble()) * 100.toDouble()).toInt()
//                                var response = ApiAdapter.retrofit.setAnswerForAttempt(hash, pokusaj.id, OdgovorBody(odgovor, pitanje.id, bodovi))
//                                return@withContext bodovi
//                            }
//                            else{
//                                bodovi = ((brojTacnih.toDouble() / pitanja.size.toDouble()) * 100.toDouble()).toInt()
//                                var response = ApiAdapter.retrofit.setAnswerForAttempt(hash, pokusaj.id, OdgovorBody(odgovor, pitanje.id, bodovi))
//                                return@withContext bodovi
//                            }
//                        }
//                    }
//                }
//            }
            return@withContext -1
        }
    }

        suspend fun getOdgovoriKviz(idKviza: Int): List<Odgovor> {
            return withContext(Dispatchers.IO) {
                var hash = AccountRepository.getHash()
                var pokusaji = ApiAdapter.retrofit.getAttempts(hash)
                for (pokusaj in pokusaji) {
                    if (pokusaj.KvizId == idKviza) {
                        var response = ApiAdapter.retrofit.getAnswersForAttempt(hash, pokusaj.id) //nije dobro
                        if (response.size == 0) {
                            return@withContext listOf<Odgovor>()
                        }
                        return@withContext response
                    }
                }
                return@withContext listOf<Odgovor>()
            }
        }

        suspend fun dajOdgovore(idKviza: Int) : List<Odgovor>{
            var db = AppDatabase.getInstance(context)
            var odgovori = db.odgovorDao().getAll()
            var odgovoriPovrat = mutableListOf<Odgovor>()
            var hash = AccountRepository.getHash()
            var pokusaji = ApiAdapter.retrofit.getAttempts(hash)
            for (pokusaj in pokusaji) {
                if (pokusaj.KvizId == idKviza) {
                    for(odgovor in odgovori){
                        if(odgovor.kvizTakenId == pokusaj.id) odgovoriPovrat.add(odgovor)
                    }
                }
            }
            return odgovoriPovrat
        }

        suspend fun postaviOdgovorUTabelu(idKvizTaken:Int, idPitanje:Int, odgovor:Int) : Boolean{
            var db = AppDatabase.getInstance(context)
            var odgovori = db.odgovorDao().getAll()
            var maxId = 0
            for(odgovor1 in odgovori){
                if(odgovor1.id > maxId) maxId = odgovor1.id
                if(odgovor1.pitanjeId == idPitanje){

                   return false
                }
            }
            val odgovorVar: Odgovor = Odgovor(maxId+1, odgovor, idKvizTaken, idPitanje)
            db.odgovorDao().insertAll(odgovorVar)
            return true
        }

        suspend fun predajOdgovore(idKviz: Int){
            var hash = AccountRepository.getHash()
            var db = AppDatabase.getInstance(context)

            val pokusaji = TakeKvizRepository.getPocetiKvizovi()
            if (pokusaji != null) {
                for(pokusaj in pokusaji){
                    if(pokusaj.KvizId == idKviz){
                        var pitanja = db.pitanjeDao().getPitanjaZaKviz(pokusaj.KvizId)
                        var odgovori = db.odgovorDao().getAll()
                        var bodovi: Int = 0
                        var brojTacnih: Int = 0
                        for (odgovor in odgovori) {
                            for (pitanje in pitanja) {
                                if (odgovor.pitanjeId == pitanje.id) {
                                    if (odgovor.odgovoreno == pitanje.tacan) brojTacnih++
                                }
                            }
                        }
                        bodovi = ((brojTacnih.toDouble() / pitanja.size.toDouble()) * 100.toDouble()).toInt()

                        for (odgovor in odgovori) {
                            for (pitanje in pitanja) {
                                if (odgovor.pitanjeId == pitanje.id) {
                                    ApiAdapter.retrofit.setAnswerForAttempt(hash, pokusaj.id, OdgovorBody(odgovor.odgovoreno, pitanje.id, bodovi))
                                }
                            }
                        }
                        db.kvizDao().postaviPredan(idKviz)
                        db.kvizDao().postaviOsvojeneBodove(bodovi, idKviz)

                        val datumRada = formatirajDatum(Calendar.getInstance().time)
                        db.kvizDao().postaviDatum(datumRada, idKviz)


                        uradjenNoviKviz = true
                    }
                }
            }
        }
        fun formatirajDatum(date: Date) : String{
            val povrat: String
            val format1 = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            povrat = format1.format(date)
            return povrat
        }
    }



}