package ba.etf.rma21.projekat.view

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import androidx.core.os.HandlerCompat.postDelayed
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Odgovor
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository
import ba.etf.rma21.projekat.viewmodel.KvizListViewModel
import ba.etf.rma21.projekat.viewmodel.PitanjaViewModel
import ba.etf.rma21.projekat.viewmodel.SharedViewModel


class FragmentPitanje(
        private val pitanje:Pitanje
) : Fragment() {


    private lateinit var textView: TextView
    private lateinit var listView: ListView
    private lateinit var opcije: List<String>
    private lateinit var listViewArrayAdapter : ArrayAdapter<String>
    private var pitanjaViewModel = PitanjaViewModel()
    private var predan : Boolean = false
    private var kvizListViewModel = KvizListViewModel()
    private lateinit var odgovori: List<Odgovor>
    var odgovoreno = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var idKviza : Int = arguments?.get("idKviza") as Int
        pitanjaViewModel.dajOdgovore(onSuccess = ::onSuccessOdgovori, onError = ::onError, idKviza = idKviza)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        var view =  inflater.inflate(R.layout.pitanje_fragment, container, false)
        textView = view.findViewById(R.id.tekstPitanja)
        listView = view.findViewById(R.id.odgovoriLista)
        textView.setText(pitanje.tekstPitanja)
        opcije = pitanjaViewModel.dajOpcije(pitanje)
        listViewArrayAdapter = activity?.let { ArrayAdapter(it,android.R.layout.simple_list_item_1 ,opcije) }!!
        listView.adapter = listViewArrayAdapter

        if(kvizListViewModel.dajUradjen(arguments?.get("nazivKviza").toString())) predan = true

        var idKviza : Int = arguments?.get("idKviza") as Int
        var idPokusaja : Int = arguments?.get("idPokusaja") as Int

        //pitanjaViewModel.dajOdgovore(onSuccess = ::onSuccessOdgovori, onError = ::onError, idKviza = idKviza)

//        val odgovor:Int = dajUradjeno(pitanje)

        listView.onItemClickListener = object :
                AdapterView.OnItemClickListener{
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    val model = ViewModelProvider(requireActivity()).get(SharedViewModel::class.java)
                    if (position == pitanje.tacan) {
                        listView[position].setBackgroundColor(Color.parseColor("#3DDC84"))
                        model.postaviZelenu()
                    } else {
                        listView[position].setBackgroundColor(Color.parseColor("#DB4F3D"))
                        listView[pitanje.tacan].setBackgroundColor(Color.parseColor("#3DDC84"))
                        model.postaviCrvenu()
                    }
                    listView.isEnabled = false
                    if(!odgovoreno){
                        pitanjaViewModel.postaviUradjeno(onSuccess = ::onSuccessPostavi, onError = ::onError,
                        kvizTakenId = idPokusaja, pitanje = pitanje, brojOdgovora = position)
                    }

        }

//            listView.onItemClickListener = OnItemClickListener { parent, view, position, id ->
//                val model = ViewModelProvider(requireActivity()).get(SharedViewModel::class.java)
//                if (position == pitanje.tacan) {
//                    listView[position].setBackgroundColor(Color.parseColor("#3DDC84"))
//                    model.postaviZelenu()
//                } else {
//                    listView[position].setBackgroundColor(Color.parseColor("#DB4F3D"))
//                    listView[pitanje.tacan].setBackgroundColor(Color.parseColor("#3DDC84"))
//                    model.postaviCrvenu()
//                }
//                listView.isEnabled = false
//                if(!odgovoreno){
//
//                }
                //Toast.makeText(activity, "text", Toast.LENGTH_SHORT).show()
                //val model = ViewModelProvider(requireActivity()).get(SharedViewModel::class.java)
//                if (odgovor == -1) {
//                    if (position == pitanje.tacan) {
//                        listView[position].setBackgroundColor(Color.parseColor("#3DDC84"))
//                        model.postaviZelenu()
//                    } else {
//                        listView[position].setBackgroundColor(Color.parseColor("#DB4F3D"))
//                        listView[pitanje.tacan].setBackgroundColor(Color.parseColor("#3DDC84"))
//                        model.postaviCrvenu()
//                    }
//
//                    var idPokusaja : Int = arguments?.get("idPokusaja") as Int
//
//                    pitanjaViewModel.postaviUradjeno(onSuccess = ::onSuccessPostaviUradjeno, onError = ::onError, kvizTakenId = idPokusaja,
//                    pitanje = pitanje, brojOdgovora = position)
//
//                    listView.isClickable = false
//                    listView.isEnabled = false
//                } else {
//
//                    if (odgovor == pitanje.tacan) {
//                        listView[odgovor].setBackgroundColor(Color.parseColor("#3DDC84"))
//                        //model.postaviZelenu()
//                    } else {
//                        listView[odgovor].setBackgroundColor(Color.parseColor("#DB4F3D"))
//                        listView[pitanje.tacan].setBackgroundColor(Color.parseColor("#3DDC84"))
//                        //model.postaviCrvenu()
//                    }
//                }
            }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        if(predan){
            listView.isEnabled = false
        }

        var idKviza : Int = arguments?.get("idKviza") as Int

        pitanjaViewModel.dajOdgovore(onSuccess = ::onSuccessDajOdgovore, onError = ::onError, idKviza = idKviza)

//        val odgovor:Int = dajUradjeno(pitanje)
//        if(odgovor != -1){
//            Handler().postDelayed({
//                if(odgovor != -1){
//                    listView.performItemClick(listView, odgovor, listView.adapter.getItemId(odgovor))
//                }
//            }, 1)
//        }
    }

    fun onSuccessDajOdgovore(odgovoriPovrat: List<Odgovor>){
        odgovori = odgovoriPovrat
        var odgovorBroj:Int = -1
        for(odgovor in odgovoriPovrat){
            if(odgovor.pitanjeId == pitanje.id){
                odgovorBroj = odgovor.odgovoreno
                Handler().postDelayed({
                    if(odgovorBroj != -1 && odgovorBroj != -2){
//                        val toast = Toast.makeText(context, "ODGOVORENO", Toast.LENGTH_SHORT)
//                        toast.show()
                        odgovoreno = true
                        listView.performItemClick(listView, odgovorBroj, listView.adapter.getItemId(odgovorBroj))
                        listView.isEnabled = false
                    }
                    else if(odgovorBroj == -1){
//                        val toast = Toast.makeText(context, "NIJE ODGOVORENO", Toast.LENGTH_SHORT)
//                        toast.show()
                        listView.isEnabled = true
                    }
                    else if(odgovorBroj == -2){
                        listView[pitanje.tacan].setBackgroundColor(Color.parseColor("#3DDC84"))
                    }
                }, 1)
            }
        }

    }

    fun onSuccessOdgovori(odgovoriPovrat: List<Odgovor>){
        odgovori = odgovoriPovrat
    }
    fun onError() {
        val toast = Toast.makeText(context, "error", Toast.LENGTH_SHORT)
        toast.show()
    }
    fun dajUradjeno(pitanje:Pitanje) : Int{
        for(odgovor in odgovori){
            if(odgovor.pitanjeId == pitanje.id) return odgovor.odgovoreno
        }
        return -1
    }
    fun onSuccessPostaviUradjeno(int: Int){

    }

    companion object {
        fun newInstance(pitanje : Pitanje): FragmentPitanje = FragmentPitanje(pitanje)
    }

    fun onSuccessPostavi(int: Int){
//        arguments?.putString("bodovi", int.toString())
        val model = ViewModelProvider(requireActivity()).get(SharedViewModel::class.java)
        model.postaviBodove(int)
    }


}