package ba.etf.rma21.projekat.view

import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Odgovor
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.repositories.OdgovorRepository
import ba.etf.rma21.projekat.viewmodel.KvizListViewModel
import ba.etf.rma21.projekat.viewmodel.PitanjaViewModel
import ba.etf.rma21.projekat.viewmodel.SharedViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import kotlin.math.round

class FragmentPokusaj (private var pitanja: List<Pitanje>): Fragment() {

    private lateinit var navigationView:NavigationView
    private lateinit var bottomNavigationView: BottomNavigationView
    private lateinit var sharedViewModel: SharedViewModel
    private var pozicija :Int = 0
    private var brojTacnihOdgovora :Int = 0
    private var brojNetacnihOdgovora :Int = 0
    private var brojPitanja = pitanja.size
    private var kvizListViewModel = KvizListViewModel()
    private var pitanjaViewModel = PitanjaViewModel()
    private var predan:Boolean = false
    private var zaustavljen:Boolean = false
    private var istekaoRok:Boolean = false
    private lateinit var odgovori: List<Odgovor>
    private lateinit var statusKviza: String
    private lateinit var listaOdgovora: MutableList<Int>

    private val mOnNavigationItemSelectedListener = NavigationView.OnNavigationItemSelectedListener {item ->
        pozicija = item.itemId
        if(pozicija != brojPitanja) {
            var bundle2 = Bundle()
            var idKviza : Int = arguments?.get("idKviza") as Int
            var idPokusaja : Int = arguments?.get("idPokusaja") as Int
            bundle2.putInt("idPokusaja", idPokusaja)
            bundle2.putInt("idKviza", idKviza)
            val fragmentPitanje
                    : FragmentPitanje = FragmentPitanje.newInstance(pitanja.get(item.itemId))
            fragmentPitanje.arguments = bundle2
            openFragment(fragmentPitanje)
            return@OnNavigationItemSelectedListener true
            false
        }
        else{
            var idKviza : Int = arguments?.get("idKviza") as Int
            var idPokusaja : Int = arguments?.get("idPokusaja") as Int
            var bundle = Bundle()
            var bodovi2 = dajBodove()
            var procenatTacnosti : Int = ((brojTacnihOdgovora.toDouble() / brojPitanja.toDouble())*100.toDouble()).toInt()
            bundle.putString("nazivKviza", arguments?.get("nazivKviza").toString())
            bundle.putString("procenatTacnosti", procenatTacnosti.toString())
            bundle.putString("kviz", "da")
            bundle.putString("predmet", "ne")
            bundle.putInt("idPokusaja", idPokusaja)
            bundle.putInt("idKviza", idKviza)
            val porukaFragment = FragmentPoruka.newInstance()
            porukaFragment.arguments = bundle
            val fragmentManager: FragmentManager? = getFragmentManager()
            if (fragmentManager != null) {
                fragmentManager.beginTransaction().replace(R.id.container, porukaFragment).commit()
            }
            return@OnNavigationItemSelectedListener true
            false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)

        listaOdgovora = mutableListOf()
        for(pitanje in pitanja){
            listaOdgovora.add(-1)
        }

        var idKviza : Int = arguments?.get("idKviza") as Int

        pitanjaViewModel.dajOdgovore(onSuccess = ::onSuccessOdgovoriOC, onError = ::onError, idKviza = idKviza)
        kvizListViewModel.dajStatusKviza(onSuccess = ::onSuccessStatus, onError = ::onError, idKviza = idKviza)
//        val model = ViewModelProvider(requireActivity()).get(SharedViewModel::class.java)
//        model.boja.observe(this, Observer<String> {item ->
//            var zaustavljenPocetniUradjen = false
//            //if(pitanjaViewModel.dajUradjeno(pitanja[pozicija]) != -1) zaustavljenPocetniUradjen = true
//            if(dajUradjeno(pitanja[pozicija])!=-1) zaustavljenPocetniUradjen = true
//            if(!predan) {
//                if(!zaustavljen || (zaustavljen && !zaustavljenPocetniUradjen)) {
//                    var menuItem = navigationView.menu.getItem(pozicija)
//                    var naziv = menuItem.title
//                    var spannableString = SpannableString(naziv)
//                    spannableString.setSpan(ForegroundColorSpan(Color.parseColor(item)), 0, spannableString.length, 0)
//                    menuItem.title = spannableString
//                    if (item == "#3DDC84") brojTacnihOdgovora++
//                    if (item == "#DB4F3D") brojNetacnihOdgovora++
//                    if(brojTacnihOdgovora + brojNetacnihOdgovora == brojPitanja) predajKviz()
//                }
//            }
//        } )
//        if(predan){
//            obojiMenu()
//            bottomNavigationView.menu.findItem(R.id.kvizovi).isVisible = true
//            bottomNavigationView.menu.findItem(R.id.predmeti).isVisible = true
//            bottomNavigationView.menu.findItem(R.id.predajKviz).isVisible = false
//            bottomNavigationView.menu.findItem(R.id.zaustaviKviz).isVisible = false
//        }
    }

    fun onCreatePomocna(){
        var aktivnostMain = activity as MainActivity
        bottomNavigationView = aktivnostMain.getBottomNavigationView()
        if(odgovori.size == pitanja.size){
            predan = true
        }
        if(predan){
            popuniMenu(pitanja.size)
            obojiMenu()
            bottomNavigationView.menu.findItem(R.id.kvizovi).isVisible = true
            bottomNavigationView.menu.findItem(R.id.predmeti).isVisible = true
            bottomNavigationView.menu.findItem(R.id.predajKviz).isVisible = false
            bottomNavigationView.menu.findItem(R.id.zaustaviKviz).isVisible = false
        }
        else{
            popuniMenu(pitanja.size)
            obojiMenu()
        }
        val model = ViewModelProvider(requireActivity()).get(SharedViewModel::class.java)
        model.boja.observe(this, Observer<String> {item ->
            var zaustavljenPocetniUradjen = false
            //if(pitanjaViewModel.dajUradjeno(pitanja[pozicija]) != -1) zaustavljenPocetniUradjen = true
            if(dajUradjeno(pitanja[pozicija])!=-1) zaustavljenPocetniUradjen = true
            if(!predan) {
                if(!zaustavljen || (zaustavljen && !zaustavljenPocetniUradjen)) {
                    var menuItem = navigationView.menu.getItem(pozicija)
                    var naziv = menuItem.title
                    var spannableString = SpannableString(naziv)
                    spannableString.setSpan(ForegroundColorSpan(Color.parseColor(item)), 0, spannableString.length, 0)



                    menuItem.title = spannableString
                    if (item == "#3DDC84"){
                        brojTacnihOdgovora++
                        listaOdgovora[pozicija] = 1
                    }
                    if (item == "#DB4F3D"){
                        brojNetacnihOdgovora++
                        listaOdgovora[pozicija] = 0
                    }

                    if(provjeriZaKraj()) predajKviz()


//                    val toast = Toast.makeText(context, odgovori.size.toString(), Toast.LENGTH_SHORT)
//                    toast.show()

                    //if(brojTacnihOdgovora + brojNetacnihOdgovora == brojPitanja) predajKviz()
                    //if(odgovori.size == brojPitanja) predajKviz()
                }
            }
            var idKviza : Int = arguments?.get("idKviza") as Int

        } )

        zaustavljen = jeLiZaustavljen()

//        istekaoRok = jeLiIstekaoRok()

//        popuniMenu(pitanja.size)
        navigationView.setNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        navigationView.setBackgroundColor(Color.parseColor("#000000"))


//        if(predan){
//            popuniMenu(pitanja.size)
//            obojiMenu()
//            bottomNavigationView.menu.findItem(R.id.kvizovi).isVisible = true
//            bottomNavigationView.menu.findItem(R.id.predmeti).isVisible = true
//            bottomNavigationView.menu.findItem(R.id.predajKviz).isVisible = false
//            bottomNavigationView.menu.findItem(R.id.zaustaviKviz).isVisible = false
//        }
        //navigationView.itemTextColor = Color.parseColor("#000000")
        val fragmentPitanje : FragmentPitanje = FragmentPitanje.newInstance(pitanja.get(0))
        var bundle2 = Bundle()
        var idKviza : Int = arguments?.get("idKviza") as Int
        var idPokusaja : Int = arguments?.get("idPokusaja") as Int
        bundle2.putInt("idPokusaja", idPokusaja)
        bundle2.putInt("idKviza", idKviza)
        fragmentPitanje.arguments = bundle2
        openFragment(fragmentPitanje)

        bottomNavigationView = aktivnostMain.getBottomNavigationView()
        bottomNavigationView.menu.findItem(R.id.kvizovi).isVisible = false
        bottomNavigationView.menu.findItem(R.id.predmeti).isVisible = false
        bottomNavigationView.menu.findItem(R.id.predajKviz).isVisible = true
        bottomNavigationView.menu.findItem(R.id.zaustaviKviz).isVisible = true

        bottomNavigationView.menu.findItem(R.id.predajKviz).setOnMenuItemClickListener(predajKvizListener)
        bottomNavigationView.menu.findItem(R.id.zaustaviKviz).setOnMenuItemClickListener(zaustaviKvizListener)

        if(predan){
            obojiMenu()
            bottomNavigationView.menu.findItem(R.id.kvizovi).isVisible = true
            bottomNavigationView.menu.findItem(R.id.predmeti).isVisible = true
            bottomNavigationView.menu.findItem(R.id.predajKviz).isVisible = false
            bottomNavigationView.menu.findItem(R.id.zaustaviKviz).isVisible = false
        }
        if(zaustavljen){
            obojiMenu()
        }
        if(istekaoRok){
            obojiMenu()
            bottomNavigationView.menu.findItem(R.id.kvizovi).isVisible = true
            bottomNavigationView.menu.findItem(R.id.predmeti).isVisible = true
            bottomNavigationView.menu.findItem(R.id.predajKviz).isVisible = false
            bottomNavigationView.menu.findItem(R.id.zaustaviKviz).isVisible = false
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var idKviza : Int = arguments?.get("idKviza") as Int
//        kvizListViewModel.dajStatusKviza(onSuccess = ::onSuccessStatus, onError = ::onError, idKviza = idKviza)
        //pitanjaViewModel.dajOdgovore(onSuccess = ::onSuccessOdgovori, onError = ::onError, idKviza = idKviza)
        //pitanjaViewModel.dajOdgovore(onSuccess = ::onSuccessOdgovoriOCV, onError = ::onError, idKviza = idKviza)
        if(kvizListViewModel.dajUradjen(arguments?.get("nazivKviza").toString())) predan = true
        var view =  inflater.inflate(R.layout.pokusaj_fragment, container, false)

//        zaustavljen = jeLiZaustavljen()
//
//        istekaoRok = jeLiIstekaoRok()

        navigationView = view.findViewById(R.id.navigacijaPitanja)
//        popuniMenu(pitanja.size)
//        navigationView.setNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
//        navigationView.setBackgroundColor(Color.parseColor("#000000"))
//
//        //navigationView.itemTextColor = Color.parseColor("#000000")
//        val fragmentPitanje : FragmentPitanje = FragmentPitanje.newInstance(pitanja.get(0))
//        openFragment(fragmentPitanje)
//
//        var aktivnostMain = activity as MainActivity
//        bottomNavigationView = aktivnostMain.getBottomNavigationView()
//        bottomNavigationView.menu.findItem(R.id.kvizovi).isVisible = false
//        bottomNavigationView.menu.findItem(R.id.predmeti).isVisible = false
//        bottomNavigationView.menu.findItem(R.id.predajKviz).isVisible = true
//        bottomNavigationView.menu.findItem(R.id.zaustaviKviz).isVisible = true
//
//        bottomNavigationView.menu.findItem(R.id.predajKviz).setOnMenuItemClickListener(predajKvizListener)
//        bottomNavigationView.menu.findItem(R.id.zaustaviKviz).setOnMenuItemClickListener(zaustaviKvizListener)
//
//        if(predan){
//            obojiMenu()
//            bottomNavigationView.menu.findItem(R.id.kvizovi).isVisible = true
//            bottomNavigationView.menu.findItem(R.id.predmeti).isVisible = true
//            bottomNavigationView.menu.findItem(R.id.predajKviz).isVisible = false
//            bottomNavigationView.menu.findItem(R.id.zaustaviKviz).isVisible = false
//        }
//        if(zaustavljen){
//            obojiMenu()
//        }
//        if(istekaoRok){
//            obojiMenu()
//            bottomNavigationView.menu.findItem(R.id.kvizovi).isVisible = true
//            bottomNavigationView.menu.findItem(R.id.predmeti).isVisible = true
//            bottomNavigationView.menu.findItem(R.id.predajKviz).isVisible = false
//            bottomNavigationView.menu.findItem(R.id.zaustaviKviz).isVisible = false
//        }
        return view
    }



    companion object {
        fun newInstance(pitanja: List<Pitanje>): FragmentPokusaj = FragmentPokusaj(pitanja)
    }

    val predajKvizListener = MenuItem.OnMenuItemClickListener {
        if(!provjeriJelSveOdgovoreno()){
            predajNepotpunKviz()
            return@OnMenuItemClickListener true
        }
        predajKviz()
        return@OnMenuItemClickListener true
    }

    fun provjeriJelSveOdgovoreno() : Boolean{
        for(odgovor in listaOdgovora){
            if(odgovor == -1) return false
        }
        return true
    }

    fun predajNepotpunKviz(){
        var idPokusaja : Int = arguments?.get("idPokusaja") as Int
        for(i in 0 until pitanja.size){
            if(listaOdgovora[i] == -1){
                listaOdgovora[i] = -2
                pitanjaViewModel.postaviUradjeno(onSuccess = ::onSuccessNepotpunOdgovor, onError = ::onError, kvizTakenId = idPokusaja,
                pitanje = pitanja[i], brojOdgovora = -2)
            }
        }

    }

    fun onSuccessNepotpunOdgovor(int: Int){
        if(provjeriJelSveOdgovoreno()){
            predajKviz()
        }
    }

    val zaustaviKvizListener = MenuItem.OnMenuItemClickListener {
        zaustaviKviz()
        bottomNavigationView.menu.findItem(R.id.kvizovi).isVisible = true
        bottomNavigationView.menu.findItem(R.id.predmeti).isVisible = true
        bottomNavigationView.menu.findItem(R.id.predajKviz).isVisible = false
        bottomNavigationView.menu.findItem(R.id.zaustaviKviz).isVisible = false
        val kvizFragment = FragmentKvizovi.newInstance()
        openFragmentContainer(kvizFragment)
        return@OnMenuItemClickListener true
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        if (transaction != null) {
            transaction.replace(R.id.framePitanje, fragment)
        }
        if (transaction != null) {
            transaction.addToBackStack(null)
        }
        if (transaction != null) {
            transaction.commit()
        }
    }
    private fun popuniMenu(brojpitanja: Int){
        var menu: Menu = navigationView.getMenu()
        for(i in 1..brojpitanja){
            menu.add(0, i-1, i-1, i.toString())
        }
        if(predan || istekaoRok){
            menu.add(0, brojpitanja, brojpitanja, "Rezultat")
        }
    }
    private fun openFragmentContainer(fragment: Fragment){
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        if (transaction != null) {
            transaction.replace(R.id.container, FragmentKvizovi.newInstance())
        }
        if (transaction != null) {
            transaction.addToBackStack(null)
        }
        if (transaction != null) {
            transaction.commit()
        }
    }

    private fun predajKviz(){
        predan = true

        var idKviza : Int = arguments?.get("idKviza") as Int
        pitanjaViewModel.predajOdgovore(onSuccess = ::onSuccessPredajOdgovore, onError = ::onError, idKviza = idKviza)

        var idPokusaja : Int = arguments?.get("idPokusaja") as Int
        pitanjaViewModel.getBodoviZaKviz(onSuccess = ::onSuccessPredaj, onError = ::onError, idPokusaja = idPokusaja)


//        var procenatTacnosti : Double = zaokruziNaDvijeDecimale(brojTacnihOdgovora.toDouble() / brojPitanja.toDouble())
//        //kvizListViewModel.postaviUradjen(arguments?.get("nazivKviza").toString(), procenatTacnosti.toFloat())
//        var bundle = Bundle()
//        var idKviza : Int = arguments?.get("idKviza") as Int
//        var idPokusaja : Int = arguments?.get("idPokusaja") as Int
//        bundle.putString("nazivKviza", arguments?.get("nazivKviza").toString())
//        bundle.putString("procenatTacnosti", procenatTacnosti.toString())
//        bundle.putString("kviz", "da")
//        bundle.putString("predmet", "ne")
//        bundle.putInt("idKviza", idKviza)
//        bundle.putInt("idPokusaja", idPokusaja)
//
//        val porukaFragment = FragmentPoruka.newInstance()
//        porukaFragment.arguments = bundle
//        val fragmentManager: FragmentManager? = getFragmentManager()
//        if (fragmentManager != null) {
//            fragmentManager.beginTransaction().replace(R.id.container, porukaFragment).commit()
//        }
//        var aktivnostMain = activity as MainActivity
//        bottomNavigationView = aktivnostMain.getBottomNavigationView()
//        bottomNavigationView.menu.findItem(R.id.kvizovi).isVisible = true
//        bottomNavigationView.menu.findItem(R.id.predmeti).isVisible = true
//        bottomNavigationView.menu.findItem(R.id.predajKviz).isVisible = false
//        bottomNavigationView.menu.findItem(R.id.zaustaviKviz).isVisible = false
    }

    fun onSuccessPredajOdgovore(){

    }

    fun onSuccessPredaj(bodovi : Int){

        val model = ViewModelProvider(requireActivity()).get(SharedViewModel::class.java)

        var bodovi2 = dajBodove()

        var bundle = Bundle()
        var idKviza : Int = arguments?.get("idKviza") as Int
        var idPokusaja : Int = arguments?.get("idPokusaja") as Int
        bundle.putString("nazivKviza", arguments?.get("nazivKviza").toString())
        bundle.putString("procenatTacnosti", bodovi2.toString())
        bundle.putString("kviz", "da")
        bundle.putString("predmet", "ne")
        bundle.putInt("idKviza", idKviza)
        bundle.putInt("idPokusaja", idPokusaja)

        val porukaFragment = FragmentPoruka.newInstance()
        porukaFragment.arguments = bundle
        val fragmentManager: FragmentManager? = getFragmentManager()
        if (fragmentManager != null) {
            fragmentManager.beginTransaction().replace(R.id.container, porukaFragment).commit()
        }
        var aktivnostMain = activity as MainActivity
        bottomNavigationView = aktivnostMain.getBottomNavigationView()
        bottomNavigationView.menu.findItem(R.id.kvizovi).isVisible = true
        bottomNavigationView.menu.findItem(R.id.predmeti).isVisible = true
        bottomNavigationView.menu.findItem(R.id.predajKviz).isVisible = false
        bottomNavigationView.menu.findItem(R.id.zaustaviKviz).isVisible = false
    }

    private fun zaustaviKviz(){
        zaustavljen = true
    }

    private fun gotovo() : Boolean{
        return false
    }

    private fun obojiMenu(){
        brojTacnihOdgovora = 0
        brojNetacnihOdgovora = 0
        for(i in 0 until brojPitanja){
            val odgovor = dajUradjeno(pitanja.get(i))
            if(odgovor == pitanja.get(i).tacan){
                var menuItem = navigationView.menu.getItem(i)
                var naziv = menuItem.title
                var spannableString = SpannableString(naziv)
                spannableString.setSpan(ForegroundColorSpan(Color.parseColor("#3DDC84")), 0, spannableString.length, 0)
                menuItem.title = spannableString
                brojTacnihOdgovora++
                continue
            }
            else if(odgovor != -1 && odgovor != pitanja.get(i).tacan){
                var menuItem = navigationView.menu.getItem(i)
                var naziv = menuItem.title
                var spannableString = SpannableString(naziv)
                spannableString.setSpan(ForegroundColorSpan(Color.parseColor("#DB4F3D")), 0, spannableString.length, 0)
                menuItem.title = spannableString
                brojNetacnihOdgovora++
                continue
            }
            else if(odgovor == -1 && !istekaoRok){
                continue
            }
            else if(odgovor == -1 && istekaoRok){
                var menuItem = navigationView.menu.getItem(i)
                var naziv = menuItem.title
                var spannableString = SpannableString(naziv)
                spannableString.setSpan(ForegroundColorSpan(Color.parseColor("#DB4F3D")), 0, spannableString.length, 0)
                menuItem.title = spannableString
                continue
            }
        }
    }
    private fun jeLiZaustavljen():Boolean{
        if(!predan){
            for(pitanje in pitanja){
                if(dajUradjeno(pitanje) != -1) return true
            }
        }
        return false
    }
    private fun jeLiIstekaoRok():Boolean{
        var idKviza : Int = arguments?.get("idKviza") as Int
        if(statusKviza == "Prošao") return true
        return false
    }

    private fun zaokruziNaDvijeDecimale(broj: Double) : Double{
        val number:Double = broj
        val number3digits:Double = Math.round(number * 1000.0) / 1000.0
        val number2digits:Double = Math.round(number3digits * 100.0) / 100.0
        return number2digits
    }
    fun onSuccessOdgovoriOC(odgovoriPovrat: List<Odgovor>){
        odgovori = odgovoriPovrat
        onCreatePomocna()
    }
    fun onSuccessOdgovori(odgovoriPovrat: List<Odgovor>){
        odgovori = odgovoriPovrat
    }
    fun onSuccessStatus(status: String){
        statusKviza = status
    }
    fun onError() {
        val toast = Toast.makeText(context, "error", Toast.LENGTH_SHORT)
        toast.show()
    }
    fun dajUradjeno(pitanje:Pitanje) : Int{
        for(odgovor in odgovori){
            if(odgovor.pitanjeId == pitanje.id) return odgovor.odgovoreno
        }
        return -1
    }
    fun provjeriZaKraj() : Boolean{
        for(odgovor in listaOdgovora){
            if(odgovor == -1) return false
        }
        return true
    }
    fun dajBodove() : Int{
        var brojTacnih:Int = 0
        for(odgovor in listaOdgovora){
            if(odgovor == 1) brojTacnih++
        }
        var bodovi = ((brojTacnih.toDouble() / pitanja.size.toDouble()) * 100.toDouble()).toInt()
        return bodovi
    }
}