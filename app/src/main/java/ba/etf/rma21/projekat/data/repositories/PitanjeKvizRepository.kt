package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.models.PitanjeKviz
import ba.etf.rma21.projekat.data.staticdata.getPitanjeKviz
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PitanjeKvizRepository {
    companion object {

        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }

        init {

        }
        

        val listaKvizPitanja : MutableList<PitanjeKviz> = getPitanjeKviz().toMutableList()

        suspend fun getPitanja(idKviza:Int):List<Pitanje>{
            return withContext(Dispatchers.IO) {
                if(DBRepository.updateNow() == false){
                    var db = AppDatabase.getInstance(context)
                    var pitanja: List<Pitanje> = db.pitanjeDao().getPitanjaZaKviz(idKviza)
                    return@withContext pitanja

                }
                var db = AppDatabase.getInstance(context)
                var pitanja: List<Pitanje> = db.pitanjeDao().getPitanjaZaKviz(idKviza)
                return@withContext pitanja
            }
        }
        suspend fun getPitanjaWS(idKviza:Int):List<Pitanje>{
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getQuestionsForKviz(idKviza)
                for(pitanje in response){
                    pitanje.idKviza = idKviza
                }
                return@withContext response
            }
        }

//        fun getPitanja(nazivKviza: String, nazivPredmeta: String): List<Pitanje> {
//            val listaPitanja = svaPitanja()
//            val listaPitanjaKviz = listaKvizPitanja
//            val listaPitanjaPovrat: MutableList<Pitanje> = mutableListOf()
//            val kviz: Kviz? = KvizRepository.getKvizZaPredmet(nazivKviza, nazivPredmeta)
//            for (pitanje in listaPitanja) {
//                for (pitanjekviz in listaPitanjaKviz) {
//                    if (kviz != null) {
//                        if (pitanje.naziv == pitanjekviz.naziv && nazivKviza == pitanjekviz.kviz && kviz.naziv == pitanjekviz.kviz)
//                            listaPitanjaPovrat.add(pitanje)
//                    }
//                }
//            }
//            return listaPitanjaPovrat.toList()
//        }


        fun dajOpcije(pitanje: Pitanje) : List<String>{
            return pitanje.opcije
        }

        fun postaviUradjeno(pitanje: Pitanje, brojOdgovora: Int){
            for(pitanjekviz in listaKvizPitanja){
                if(pitanjekviz.naziv == pitanje.naziv) pitanjekviz.odgovoren = brojOdgovora
            }
        }
        fun dajUradjeno(pitanje: Pitanje) : Int{

            val listaPitanjaKviz = listaKvizPitanja
            for(pitanjekviz in listaPitanjaKviz){
                if(pitanje.naziv == pitanjekviz.naziv){
                return pitanjekviz.odgovoren
                }
            }
            return -1
        }
    }



}