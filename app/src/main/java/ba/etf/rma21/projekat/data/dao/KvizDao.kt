package ba.etf.rma21.projekat.data.dao

import androidx.room.*
import ba.etf.rma21.projekat.data.models.Kviz

@Dao
interface KvizDao {
    @Query("SELECT * FROM kviz")
    suspend fun getAll() : List<Kviz>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg kvizovi: Kviz)


//
//    @Delete
//    suspend fun deleteAll()

    @Query("DELETE FROM kviz")
    suspend fun deleteAll()


    @Query("UPDATE kviz SET predan=1 WHERE id=:idKviza")
    suspend fun postaviPredan(idKviza: Int)

    @Query("UPDATE kviz SET datumRada=:date WHERE id=:idKviza")
    suspend fun postaviDatum(date: String, idKviza: Int)

    @Query("UPDATE kviz SET osvojeniBodovi=:bodovi WHERE id=:idKviza")
    suspend fun postaviOsvojeneBodove(bodovi:Int, idKviza: Int)
}