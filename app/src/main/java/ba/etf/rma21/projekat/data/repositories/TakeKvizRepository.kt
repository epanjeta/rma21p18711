package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.viewmodel.SharedViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class TakeKvizRepository {

    companion object {

        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }

        suspend fun zapocniKviz(idKviza: Int): KvizTaken? {
            return withContext(Dispatchers.IO) {

                setContext(OdgovorRepository.getContext())

                var hash = AccountRepository.getHash()
                var pokusaji = ApiAdapter.retrofit.getAttempts(hash)
                for (pokusaj in pokusaji) {
                    if (pokusaj.KvizId == idKviza) {
                        return@withContext pokusaj
                    }
                }
                var response = ApiAdapter.retrofit.startAttempt(hash, idKviza)
                pokusaji = ApiAdapter.retrofit.getAttempts(hash)
                for (pokusaj in pokusaji) {
                    if (pokusaj.KvizId == idKviza) {
                        var db = AppDatabase.getInstance(context)
                        db.kvizTakenDao().insertAll(KvizTaken(pokusaj.id, pokusaj.student, pokusaj.osvojeniBodovi, pokusaj.datumRada, pokusaj.KvizId, 0))
                        return@withContext KvizTaken(pokusaj.id, pokusaj.student, pokusaj.osvojeniBodovi, pokusaj.datumRada, pokusaj.KvizId, 0)
                    }
                }
                return@withContext null
            }
        }

        suspend fun getPocetiKvizovi(): List<KvizTaken>? {
            return withContext(Dispatchers.IO) {
                var hash = AccountRepository.getHash()
                var response = ApiAdapter.retrofit.getAttempts(hash)
                if(response.size != 0)
                return@withContext response
                else return@withContext null
            }
        }

        suspend fun getPocetiKvizoviBaza() : List<KvizTaken>?{
            var db = AppDatabase.getInstance(context)
            return db.kvizTakenDao().getAll()
        }

        suspend fun getBodoviZaKviz(idPokusaja:Int):Int{
            return withContext(Dispatchers.IO) {
                var hash = AccountRepository.getHash()
                var pokusaji = ApiAdapter.retrofit.getAttempts(hash)
                for(pokusaj in pokusaji){
                    if(pokusaj.id == idPokusaja) return@withContext pokusaj.osvojeniBodovi
                }
                return@withContext 0
            }
        }
    }
}