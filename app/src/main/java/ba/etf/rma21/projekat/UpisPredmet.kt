//package ba.etf.rma21.projekat
//
//import android.app.Activity
//import android.content.Intent
//import androidx.appcompat.app.AppCompatActivity
//import android.os.Bundle
//import android.view.View
//import android.widget.AdapterView
//import android.widget.ArrayAdapter
//import android.widget.Button
//import android.widget.Spinner
//import ba.etf.rma21.projekat.data.models.Grupa
//import ba.etf.rma21.projekat.data.models.Predmet
//import ba.etf.rma21.projekat.viewmodel.UpisPredmetaViewModel
//
//class UpisPredmet : AppCompatActivity(), AdapterView.OnItemSelectedListener  {
//    private var upisPredmet = UpisPredmetaViewModel()
//
//    private lateinit var spinnerGodina : Spinner
//    lateinit var spinnerGodinaArrayAdapter : ArrayAdapter<CharSequence>
//
//    private lateinit var spinnerPredmeti : Spinner
//    lateinit var spinnerPredmetiArrayAdapter : ArrayAdapter<Predmet>
//
//    private lateinit var spinnerGrupa : Spinner
//    lateinit var spinnerGrupaArrayAdapter : ArrayAdapter<Grupa>
//
//    private lateinit var buttonDodaj : Button
//
//    private var odabranaGodina : Boolean = false
//    private var odabranPredmet : Boolean = false
//    private var odabranaGrupa : Boolean = false
//
//    private var klikGodina : Int = 0
//    private var klikPredmet : Int = 0
//    private var klikGrupa : Int = 0
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_upis_predmet)
//
//        spinnerGodina = findViewById(R.id.odabirGodina)
//        spinnerPredmeti = findViewById(R.id.odabirPredmet)
//        spinnerGrupa = findViewById(R.id.odabirGrupa)
//
//        buttonDodaj = findViewById(R.id.dodajPredmetDugme)
//
//        spinnerGodinaArrayAdapter = ArrayAdapter.createFromResource(this, R.array.godine, android.R.layout.simple_spinner_item)
//        spinnerGodinaArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//        spinnerGodina.setAdapter(spinnerGodinaArrayAdapter)
//        spinnerGodina.setOnItemSelectedListener(this)
//
//        val godinaZaPamtiti = intent.getIntExtra("godinaPamtiti", 1)
//        spinnerGodina.setSelection(godinaZaPamtiti-1)
//
//        buttonDodaj.isEnabled = false
//        buttonDodaj.isClickable = false
//
//        buttonDodaj.setOnClickListener(object: View.OnClickListener {
//            override fun onClick(view: View): Unit {
//                upisiPredmet()
//                val resultIntent = Intent()
//                val godina:Int = spinnerGodina.getSelectedItem().toString().toInt()
//                resultIntent.putExtra("godina_za_postaviti", godina.toString())
//                setResult(Activity.RESULT_OK, resultIntent)
//                finish()
//            }
//        })
//    }
//
//    override fun onNothingSelected(parent: AdapterView<*>?) {
//
//    }
//
//    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//        if (parent != null) {
//            if(parent.id == R.id.odabirGodina){
//
//                val text = parent!!.getItemAtPosition(position).toString()
//                //Toast.makeText(parent!!.context, text, Toast.LENGTH_SHORT).show()
//                val godina = text.toInt()
////                var listaPredmeta : Array<Predmet>
////                listaPredmeta = upisPredmet.getPredmetsGodina(godina).toTypedArray()
////                var adapter: ArrayAdapter<Predmet> = ArrayAdapter(this,
////                    android.R.layout.simple_spinner_item, listaPredmeta)
////                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//                var listaPredmeta:Array<String> = dajNazivePredmeta(upisPredmet.getPredmetsGodinaNeUpisani(godina))
//                listaPredmeta = dodajPrazanString(listaPredmeta)
//                var adapter: ArrayAdapter<String> = ArrayAdapter(this,
//                        android.R.layout.simple_spinner_item, listaPredmeta)
//                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//                spinnerPredmeti.setAdapter(adapter)
//                spinnerPredmeti.setOnItemSelectedListener(this)
//            }
//            else if(parent.id == R.id.odabirPredmet){
//
//                val text = parent!!.getItemAtPosition(position).toString()
////                var listaGrupa : Array<Grupa>
////                listaGrupa = upisPredmet.getGrupePredmet(text).toTypedArray()
////                var adapter: ArrayAdapter<Grupa> = ArrayAdapter(this,
////                    android.R.layout.simple_spinner_item, listaGrupa)
////                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//                var listaGrupa:Array<String> = dajNaziveGrupa(upisPredmet.getGrupePredmet(text))
//                listaGrupa = dodajPrazanString(listaGrupa)
//                var adapter: ArrayAdapter<String> = ArrayAdapter(this,
//                        android.R.layout.simple_spinner_item, listaGrupa)
//                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//                spinnerGrupa.setAdapter(adapter)
//                spinnerGrupa.setOnItemSelectedListener(this)
//
//            }
//            else if(parent.id == R.id.odabirGrupa){
////                klikGrupa++
////                if(klikGodina > 1 && klikPredmet > 1 && klikGrupa > 1){
////                    buttonDodaj.isEnabled = true
////                    buttonDodaj.isClickable = true
////                }
//            }
//            if(spinnerPredmeti.getSelectedItem()!="" && spinnerGrupa.getSelectedItem()!=""){
//                buttonDodaj.isEnabled = true
//                buttonDodaj.isClickable = true
//            }
//        }
//    }
//    private fun upisiPredmet(){
//        upisPredmet.dodajPredmetUUpisane(spinnerPredmeti.getSelectedItem().toString(), spinnerGrupa.getSelectedItem().toString())
//
//    }
//    private fun dajNazivePredmeta(lista: List<Predmet>) : Array<String>{
//        val povrat: MutableList<String> = mutableListOf()
//        for(predmet in lista){
//            predmet.naziv?.let { povrat.add(it) }
//        }
//        return povrat.toTypedArray()
//    }
//    private fun dajNaziveGrupa(lista : List<Grupa>) : Array<String>{
//        val povrat: MutableList<String> = mutableListOf()
//        for(grupa in lista){
//            grupa.naziv?.let { povrat.add(it) }
//        }
//        return povrat.toTypedArray()
//    }
//    private fun dodajPrazanString(pocetniString : Array<String>): Array<String>{
//        var noviString : Array<String> = arrayOf(" ")
//        noviString.set(0, "")
//        var lista = noviString.toMutableList()
//        for(element in pocetniString){
//            lista.add(element)
//        }
//        return lista.toTypedArray()
//    }
//}
