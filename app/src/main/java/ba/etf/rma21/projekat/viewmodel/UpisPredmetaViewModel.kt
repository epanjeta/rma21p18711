package ba.etf.rma21.projekat.viewmodel

import android.util.Log
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.GrupaRepository
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class UpisPredmetaViewModel {

    val scope = CoroutineScope(Job() + Dispatchers.Main)



    fun getGrupePredmet(onSuccess: (movies: List<Grupa>) -> Unit,
                        onError: () -> Unit, nazivPredmeta: String){
        scope.launch {
            val sviPredmeti = PredmetIGrupaRepository.getPredmeti()
            var predmet:Predmet
            var result = listOf<Grupa>()
            for(predmet1 in sviPredmeti){
                if(predmet1.naziv == nazivPredmeta){
                    predmet = predmet1
                    result = PredmetIGrupaRepository.getGrupeZaPredmet(predmet.id)
                }
            }
            onSuccess?.invoke(result.toList())
        }
    }

    fun dodajPredmetUUpisane(nazivPredmeta: String, nazivGrupe: String) {

    }

    fun upisiGrupu(onSuccess: () -> Unit,
                   onError: () -> Unit, grupa: Grupa){
        scope.launch {
            PredmetIGrupaRepository.upisiUGrupu(grupa.id)
        }
    }

    fun getPredmetiZaGodinu(onSuccess: (movies: List<Predmet>) -> Unit,
                            onError: () -> Unit, godina: Int) {
        scope.launch {
            val sviPredmeti = PredmetIGrupaRepository.getPredmeti()
            val predmetiZaGodinu = mutableListOf<Predmet>()
            val result = mutableListOf<Predmet>()
            for (predmet in sviPredmeti) {
                if (predmet.godina == godina) predmetiZaGodinu.add(predmet)
            }


            val upisaneGrupe = PredmetIGrupaRepository.getUpisaneGrupe() //ERROR

            for (predmet in predmetiZaGodinu) {

                val grupePredmeta = PredmetIGrupaRepository.getGrupeZaPredmet(predmet.id)
                var upisan: Boolean = false
                for (grupa in grupePredmeta) {
                    if (upisaneGrupe.contains(grupa)) upisan = true
                }
                if (!upisan) {
                    result.add(predmet)
                }
            }

            onSuccess?.invoke(result.toList())


        }
    }
}