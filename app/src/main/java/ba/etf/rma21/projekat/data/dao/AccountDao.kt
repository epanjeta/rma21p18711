package ba.etf.rma21.projekat.data.dao

import androidx.room.*
import ba.etf.rma21.projekat.data.models.Account
import ba.etf.rma21.projekat.data.models.Kviz
import java.util.*

@Dao
interface AccountDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg account: Account)

    @Query("UPDATE account SET lastUpdate = :datumPromjene ")
    suspend fun izmijeniDatum(datumPromjene: String)

    @Query("SELECT lastUpdate from account")
    suspend fun getLastUpdate() : String

    @Query("DELETE From account")
    suspend fun deleteAll()
}