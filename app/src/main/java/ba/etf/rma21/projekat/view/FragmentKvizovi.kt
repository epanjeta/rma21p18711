package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.viewmodel.KvizListViewModel
import ba.etf.rma21.projekat.viewmodel.PitanjaViewModel


class FragmentKvizovi : Fragment(), AdapterView.OnItemSelectedListener {
    private lateinit var kvizoviAdapter: KvizListAdapter
    private lateinit var kvizovi: RecyclerView
    private var kvizListViewModel = KvizListViewModel()
    lateinit var filterKvizova : Spinner
    lateinit var spinnerArrayAdapter: ArrayAdapter<CharSequence>
    private var pitanjaViewModel = PitanjaViewModel()
    private lateinit var kvizTaken: KvizTaken
    private lateinit var pitanjaZaProslijediti: List<Pitanje>
    private lateinit var zapocetiKviz: Kviz
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        var view =  inflater.inflate(R.layout.kvizovi_fragment, container, false)
        kvizovi = view.findViewById(R.id.listaKvizova)
        kvizovi.layoutManager = GridLayoutManager(activity, 2)
        kvizoviAdapter = KvizListAdapter(listOf()) {kviz -> otvoriPokusaj(kviz)}
        kvizovi.adapter = kvizoviAdapter

        //kvizoviAdapter.updateKvizovi(kvizListViewModel.getKvizovi())
        kvizListViewModel.dajKvizove(onSuccess = ::onSuccess, onError = ::onErrorKvizovi)

        filterKvizova = view.findViewById(R.id.filterKvizova)
        //moguci problem ispod
        spinnerArrayAdapter = this.context?.let { ArrayAdapter.createFromResource(it, R.array.filterKvizova, android.R.layout.simple_spinner_item) } as ArrayAdapter<CharSequence>
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        filterKvizova.setAdapter(spinnerArrayAdapter)
        filterKvizova.setOnItemSelectedListener(this);
        return view;
    }
    companion object {
        fun newInstance(): FragmentKvizovi = FragmentKvizovi()
    }

    private fun otvoriPokusaj(kviz: Kviz){

        zapocetiKviz = kviz
        pitanjaViewModel.getPitanja(onSuccess = ::onSuccessGetPitanja, onError = ::onErrorPitanja, idKviza = kviz.id)
//        kvizListViewModel.zapocniKviz(onSuccess = ::onSuccessZapocniKviz, onError = ::onErrorKviz, kviz = kviz)

//        val bundle = Bundle()
//        bundle.putString("nazivKviza", kviz.naziv)
//        bundle.putString("nazivPredmeta", kviz.nazivPredmeta)
//        bundle.putInt("idPokusaja", kvizTaken.id)
//        bundle.putInt("idKviza", kviz.id)
//
//        if(pitanjaZaProslijediti.isNotEmpty()
//                && kvizListViewModel.dajStatusKviza(kviz) != "Budući") {
//            val pokusajFragment = FragmentPokusaj.newInstance(pitanjaZaProslijediti)
//            pokusajFragment.arguments = bundle
//            val fragmentManager: FragmentManager? = getFragmentManager()
//            if (fragmentManager != null) {
//                fragmentManager.beginTransaction().replace(R.id.container, pokusajFragment).commit()
//            }
//
//        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val text = parent!!.getItemAtPosition(position).toString()
        if(text == "Urađeni kvizovi"){
            val toast = Toast.makeText(context, "Ovo će mozda potrajati...", Toast.LENGTH_SHORT)
            toast.show()
            kvizListViewModel.getUradjeniKvizovi(onSuccess = ::onSuccess, onError = ::onErrorKvizovi)
            kvizoviAdapter.notifyDataSetChanged()

        }
        else if(text == "Prošli kvizovi"){
            val toast = Toast.makeText(context, "Ovo će mozda potrajati...", Toast.LENGTH_SHORT)
            toast.show()
            kvizListViewModel.getProsliKvizovi(onSuccess = ::onSuccess, onError = ::onErrorKvizovi)
            kvizoviAdapter.notifyDataSetChanged()
        }
        else if(text == "Budući kvizovi"){
            val toast = Toast.makeText(context, "Ovo će mozda potrajati...", Toast.LENGTH_SHORT)
            toast.show()
            kvizListViewModel.getBuduciKvizovi(onSuccess = ::onSuccess, onError = ::onErrorKvizovi)
            kvizoviAdapter.notifyDataSetChanged()
        }
        else if(text == "Svi moji kvizovi"){
            val toast = Toast.makeText(context, "Ovo će mozda potrajati...", Toast.LENGTH_SHORT)
            toast.show()
            kvizListViewModel.getUpisani(onSuccess = ::onSuccess, onError = ::onErrorKvizovi)
            kvizoviAdapter.notifyDataSetChanged()
        }
        else{
            val toast = Toast.makeText(context, "Ovo će mozda potrajati...", Toast.LENGTH_SHORT)
            toast.show()
            kvizListViewModel.dajKvizove(onSuccess = ::onSuccess, onError = ::onErrorKvizovi)
            kvizoviAdapter.notifyDataSetChanged()
        }
    }

    fun onSuccess(noviKvizovi:List<Kviz>) {
        kvizoviAdapter.updateKvizovi(noviKvizovi)

    }
    fun onSuccessUpisani(noviKvizovi:List<Kviz>) {
//        if(context != null){
//            val toast = Toast.makeText(context, "NIJE error CONTEXT", Toast.LENGTH_SHORT)
//            toast.show()
//        }
        for(kviz in noviKvizovi){
            context?.let { kvizListViewModel.insertAll(it, kviz, onSuccess = ::onSuccessInsert, onError = ::onError) }
        }

        kvizoviAdapter.updateKvizovi(noviKvizovi)


    }
    fun onSuccessInsert() {
        if(context != null){
            val toast = Toast.makeText(context, "NIJE error INSERT", Toast.LENGTH_SHORT)
            toast.show()
        }
    }
    fun onSuccessGetPitanja(pitanja:List<Pitanje>) {
        pitanjaZaProslijediti = pitanja
        kvizListViewModel.zapocniKviz(onSuccess = ::onSuccessZapocniKviz, onError = ::onErrorKviz, kviz = zapocetiKviz)
    }
    fun onSuccessZapocniKviz(kvizTaken: KvizTaken){
        this.kvizTaken = kvizTaken
        val bundle = Bundle()
        bundle.putString("nazivKviza", zapocetiKviz.naziv)
        bundle.putString("nazivPredmeta", zapocetiKviz.nazivPredmeta)
        bundle.putInt("idPokusaja", kvizTaken.id)
        bundle.putInt("idKviza", zapocetiKviz.id)

        if(pitanjaZaProslijediti.isNotEmpty()
                && kvizListViewModel.dajStatusKviza(zapocetiKviz) != "Budući") {
            val pokusajFragment = FragmentPokusaj.newInstance(pitanjaZaProslijediti)
            pokusajFragment.arguments = bundle
            val fragmentManager: FragmentManager? = getFragmentManager()
            if (fragmentManager != null) {
                fragmentManager.beginTransaction().replace(R.id.container, pokusajFragment).commit()
            }

        }
    }
    fun onErrorPitanja() {
        val toast = Toast.makeText(context, "error pitanja", Toast.LENGTH_SHORT)
        toast.show()
    }
    fun onErrorKviz() {
        kvizListViewModel.zapocniKviz(onSuccess = ::onSuccessZapocniKviz, onError = ::onErrorKviz2, kviz = zapocetiKviz)
    }
    fun onErrorKviz2(){
        val toast = Toast.makeText(context, "error kviz", Toast.LENGTH_SHORT)
        toast.show()
    }
    fun onErrorKvizovi() {
        val toast = Toast.makeText(context, "error", Toast.LENGTH_SHORT)
        toast.show()
    }
    fun onError(){
        val toast = Toast.makeText(context, "error INSERT", Toast.LENGTH_SHORT)
        toast.show()
    }
}


