package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import android.service.voice.AlwaysOnHotwordDetector
import ba.etf.rma21.projekat.data.models.Account
import ba.etf.rma21.projekat.data.models.ApiAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AccountRepository {

    companion object {

        private lateinit var context: Context
        fun setContext(_context:Context){
            context=_context
        }

        var acHash: String = "d7f854f9-44f3-45db-b663-3bdc92012b81"

//        fun postaviHash(acHash: String): Boolean {
//            this.acHash = acHash
//            if (this.acHash == acHash) return true
//            else return false
//        }

        fun getHash(): String {
            return acHash
        }

        suspend fun getAccount(): Account {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getAccountForHash(acHash)
                return@withContext response
            }
        }

        suspend fun postaviHash(payload: String?){
            acHash = payload.toString()
            DBRepository.ocistiBazu()
        }


    }
}