package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Kviz

@Dao
interface GrupaDao {

    @Query("SELECT * FROM grupa")
    suspend fun getAll() : List<Grupa>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg grupe: Grupa)

    @Query("DELETE FROM grupa")
    suspend fun deleteAll()

}