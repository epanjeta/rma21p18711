package ba.etf.rma21.projekat.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import java.text.SimpleDateFormat
import java.util.*


class KvizListAdapter (
        private var kvizovi: List<Kviz>, private val onItemClicked: (kviz:Kviz) -> Unit
) : RecyclerView.Adapter<KvizListAdapter.KvizViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KvizListAdapter.KvizViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_kviz, parent, false)
        return KvizViewHolder(view)
    }

    override fun getItemCount():Int =kvizovi.size

    override fun onBindViewHolder(holder: KvizListAdapter.KvizViewHolder, position: Int) {
        holder.naziv.text = kvizovi[position].naziv
        holder.nazivPredmeta.text = kvizovi[position].nazivPredmeta


        val context: Context = holder.statusImage.getContext()


        val c = Calendar.getInstance()

        if(kvizovi[position].datumRada == null && kvizovi[position].predan == false){

//            if(kvizovi[position].predan == true){
//                holder.datum.text = kvizovi[position].datumRada?.let { ispisiDatum(it) } //uradjen kviz - plava boja
//                var id: Int = context.getResources().getIdentifier("plava", "drawable", context.getPackageName())
//                holder.statusImage.setImageResource(id)
//            }

            if(kvizovi[position].datumPocetka != null && kvizovi[position].datumKraj == null){
                holder.datum.text = ispisiDatum(kvizovi[position].datumPocetka) //aktivan ali nije uradjen - zelena boja
                var id: Int = context.getResources().getIdentifier("zelena", "drawable", context.getPackageName())
                holder.statusImage.setImageResource(id)
            }

            else if(kvizovi[position].datumPocetka > c.time){
                holder.datum.text = ispisiDatum(kvizovi[position].datumPocetka) //tek treba biti aktivan - zuta boja
                var id: Int = context.getResources().getIdentifier("zuta", "drawable", context.getPackageName())
                holder.statusImage.setImageResource(id)
            }
            else if(kvizovi[position].datumPocetka < c.time && kvizovi[position].datumKraj!! > c.time){
                holder.datum.text = kvizovi[position].datumKraj?.let { ispisiDatum(it) } //aktivan ali nije uradjen - zelena boja
                var id: Int = context.getResources().getIdentifier("zelena", "drawable", context.getPackageName())
                holder.statusImage.setImageResource(id)
            }
            else{
                holder.datum.text = kvizovi[position].datumKraj?.let { ispisiDatum(it) } //kviz istekao i nije uradjen - crvena boja
                var id: Int = context.getResources().getIdentifier("crvena", "drawable", context.getPackageName())
                holder.statusImage.setImageResource(id)
            }
        }
        else if(kvizovi[position].datumRada != null || kvizovi[position].predan == true){
            holder.datum.text = kvizovi[position].datumRada?.let { ispisiDatum(it) } //uradjen kviz - plava boja
            var id: Int = context.getResources().getIdentifier("plava", "drawable", context.getPackageName())
            holder.statusImage.setImageResource(id)
        }


        holder.trajanje.text = kvizovi[position].trajanje.toString()
        if(kvizovi[position].osvojeniBodovi == null) {
            holder.bodovi.text = ""
        }
        else{
            holder.bodovi.text = kvizovi[position].osvojeniBodovi.toString()
        }
        holder.itemView.setOnClickListener{ onItemClicked(kvizovi[position]) }

        }

    fun updateKvizovi(kvizovi: List<Kviz>){
        this.kvizovi = kvizovi
        notifyDataSetChanged()
    }

    private fun ispisiDatum(date: Date) : String{
        val povrat: String
        val format1 = SimpleDateFormat("dd.MM.yyyy")
        povrat = format1.format(date)
        return povrat
    }



    inner class KvizViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val naziv: TextView =  itemView.findViewById(R.id.naziv)
        val nazivPredmeta: TextView =  itemView.findViewById(R.id.nazivPredmeta)
        val datum: TextView =  itemView.findViewById(R.id.datum)
        val trajanje: TextView =  itemView.findViewById(R.id.trajanje)
        val bodovi: TextView = itemView.findViewById(R.id.bodovi)
        val statusImage: ImageView = itemView.findViewById(R.id.statusImage)
    }

}


