package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Pitanje

@Dao
interface PitanjeDao {
    @Query("SELECT * FROM pitanje")
    suspend fun getAll() : List<Pitanje>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg pitanja: Pitanje)

    @Query("DELETE FROM pitanje")
    suspend fun deleteAll()

    @Query("SELECT * FROM pitanje WHERE idKviza=:idKviza")
    suspend fun getPitanjaZaKviz(idKviza: Int) : List<Pitanje>

}