package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import android.widget.Toast
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Account
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Pitanje
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.math.max

class DBRepository {
    companion object{
        private lateinit var context:Context
        fun setContext(_context: Context){
            context =_context
        }

        suspend fun updateNow() : Boolean{
            return withContext(Dispatchers.IO){
                var db = AppDatabase.getInstance(context)
                var datum = db.accountDao().getLastUpdate()
                var izmijenjeno = true
                if(datum!=null) {
                    var result = ApiAdapter.retrofit.updateNow(AccountRepository.getHash(), datum)
                    izmijenjeno = result.changed
                    if(izmijenjeno == false)
                        izmijenjeno = provjeriZaIzmjene()
                }
                if(datum == null){
                    val c1 = Calendar.getInstance()
                    c1.set(2000, 3, 15)
                    val povrat: String
                    val format1 = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                    povrat = format1.format(c1.time)

                    var result = ApiAdapter.retrofit.updateNow(AccountRepository.getHash(), povrat)
                    izmijenjeno = result.changed
                    var account = AccountRepository.getAccount()
                    var trenutniDatumDate = Calendar.getInstance().time
                    var trenutniDatumString = format1.format(trenutniDatumDate)
                    account.lastUpdate = trenutniDatumString
                    db.accountDao().insertAll(account)
                }
                if(izmijenjeno){
                    ocistiBazu()
//                    var aktivnostMain = activity as MainActivity
//                    val toast = Toast.makeText(applicationContext, "Ovo će malo potrajati", Toast.LENGTH_SHORT)
//                    toast.show()
                    var kvizovi = KvizRepository.getUpisaniWS()
                    var predmeti = PredmetIGrupaRepository.getUpisaniPredmetiWS()
                    var grupe = PredmetIGrupaRepository.getUpisaneGrupeWS()
                    var pitanja = mutableListOf<Pitanje>()
                    for(kviz in kvizovi){
                        KvizRepository.popuniPodatkeZaKviz(kviz)
                    }
                    for(kviz in kvizovi){
                        pitanja.addAll(PitanjeKvizRepository.getPitanjaWS(kviz.id))
                    }
                    for(kviz in kvizovi){
                        db.kvizDao().insertAll(kviz)
                    }
                    for(predmet in predmeti){
                        db.predmetDao().insertAll(predmet)
                    }
                    for(grupa in grupe){
                        db.grupaDao().insertAll(grupa)
                    }
                    for(pitanje in pitanja){

                        var pitanjaIzBaze = db.pitanjeDao().getAll()

                        var maxId = 0
                        var duploPitanje = false
                        for(pitanje1 in pitanjaIzBaze){
                            if(pitanje1.id > maxId) maxId = pitanje1.id
                        }
                        for(pitanje1 in pitanjaIzBaze){
                            if(pitanje.id == pitanje1.id && pitanje.idKviza != pitanje1.idKviza){
                                db.pitanjeDao().insertAll(Pitanje(maxId+1, pitanje.naziv, pitanje.tekstPitanja, pitanje.opcije, pitanje.tacan, pitanje.idKviza))
                                duploPitanje = true
                            }
                        }
                        if(!duploPitanje)
                            db.pitanjeDao().insertAll(pitanje)
                    }
                    var account = AccountRepository.getAccount()
                    var trenutniDatumDate = Calendar.getInstance().time
                    val format1 = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                    var trenutniDatumString = format1.format(trenutniDatumDate)
                    account.lastUpdate = trenutniDatumString
                    db.accountDao().insertAll(account)
                }
                return@withContext izmijenjeno
            }
        }
        suspend fun provjeriZaIzmjene() : Boolean{
            var grupe = PredmetIGrupaRepository.getUpisaneGrupeWS()
            var db = AppDatabase.getInstance(context)
            if(grupe.size != db.grupaDao().getAll().size) return true

//            var pokusajiBaza = db.kvizTakenDao().getAll()
//            var pokusajiWS = TakeKvizRepository.getPocetiKvizovi()



//            var kvizoviWS = KvizRepository.getAll()
//            var kvizoviBaza = db.kvizDao().getAll()
//            for(kviz1 in kvizoviBaza){
//                for(kviz2 in kvizoviWS){
//                    if(kviz1.id ==kviz2.id){
//                        if(kviz1.datumRada == null && kviz2.datumRada!=null) return true
//                    }
//                }
//            }

            return false
        }
        suspend fun ocistiBazu(){
            var db = AppDatabase.getInstance(context)
            db.accountDao().deleteAll()
            db.grupaDao().deleteAll()
            db.pitanjeDao().deleteAll()
            db.predmetDao().deleteAll()
            db.kvizDao().deleteAll()
            db.odgovorDao().deleteAll()
            db.kvizTakenDao().deleteAll()
        }

    }
}