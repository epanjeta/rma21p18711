package ba.etf.rma21.projekat.data

import androidx.room.TypeConverter
import java.text.SimpleDateFormat
import java.util.*

class Converters {
    companion object{
        @TypeConverter
        @JvmStatic
        fun fromDate(value: Date?) : String{

            if(value == null) return ""

            val povrat: String
            val format1 = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            povrat = format1.format(value)
            return povrat
        }
        @TypeConverter
        @JvmStatic
        fun toDate(value: String) : Date?{

            if(value == "") return null

            val date = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(value)
            return date
        }
        @TypeConverter
        @JvmStatic
        fun fromList(value: List<String>) : String{
            val string = value.joinToString(",")
            return string
        }
        @TypeConverter
        @JvmStatic
        fun toList(value: String) : List<String>{
            val lista = value.split(",")
            return lista
        }
    }


}