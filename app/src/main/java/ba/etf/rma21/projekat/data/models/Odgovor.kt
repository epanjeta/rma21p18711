package ba.etf.rma21.projekat.data.models

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
@Entity
data class Odgovor(
        @PrimaryKey @SerializedName("id") var id: Int,
        @ColumnInfo(name = "odgovoreno") @SerializedName("odgovoreno") var odgovoreno: Int,
        @ColumnInfo(name = "KvizTakenId") @SerializedName("KvizTakenId") var kvizTakenId: Int,
        @ColumnInfo(name = "PitanjeId") @SerializedName("PitanjeId") var pitanjeId: Int
)



