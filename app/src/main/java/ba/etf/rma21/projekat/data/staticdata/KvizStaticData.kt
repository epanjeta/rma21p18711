//package ba.etf.rma21.projekat.data.staticdata
//
//import ba.etf.rma21.projekat.data.models.Kviz
//import java.util.*
//
//fun sviKvizovi(): List<Kviz>{
//
//    val c1 = Calendar.getInstance()
//    c1.set(2022, 3, 20)
//    val c2 = Calendar.getInstance()
//    c2.set(2022, 3, 21)
//    val c3 = Calendar.getInstance()
//    c3.set(2021, 3, 1)
//    val c4 = Calendar.getInstance()
//    c4.set(2022, 3, 15)
//    val c5 = Calendar.getInstance()
//    c5.set(2021, 0, 1)
//    val c6 = Calendar.getInstance()
//    c6.set(2021, 0, 3)
//    val c7 = Calendar.getInstance()
//    c7.set(2021, 0, 2)
//    val c8 = Calendar.getInstance()
//    c8.set(2021, 0, 1)
//    val c9 = Calendar.getInstance()
//    c9.set(2021, 0, 2)
//
//
//    return listOf(
//        //upisani predmeti
//        Kviz("Kviz1", "DM", c1.time, c2.time, null, 15, "Grupa1", null ),
//            Kviz("Kviz2", "RMA", c3.time, c4.time, null, 15, "Grupa2", null ),
//            Kviz("Kviz3", "IM", c5.time, c6.time, c7.time, 15, "Grupa3", 1.toFloat()),
//
//            //neupisani predmeti
//            Kviz("Kviz4", "LAG", c8.time, c9.time, null, 15, "Grupa4", null),
//            Kviz("Kviz5", "LAG", c8.time, c9.time, null, 15, "Grupa3", null),
//
//            Kviz("Kviz6", "TP", c1.time, c2.time, null, 15, "Grupa1", null),
//            Kviz("Kviz7", "TP", c1.time, c2.time, null, 15, "Grupa2", null),
//
//            Kviz("Kviz8", "RPR", c5.time, c6.time, null, 15, "Grupa1", null),
//            Kviz("Kviz9", "RPR", c5.time, c6.time, null, 15, "Grupa2", null)
//    )
//
//}
//
//fun uradjeniKvizovi():List<Kviz>{
//    val c5 = Calendar.getInstance()
//    c5.set(2021, 0, 1)
//    val c6 = Calendar.getInstance()
//    c6.set(2021, 0, 3)
//    val c7 = Calendar.getInstance()
//    c7.set(2021, 0, 2)
//    return listOf(
//            Kviz("Kviz3", "IM", c5.time, c6.time, c7.time, 15, "Grupa3", 14.toFloat())
//    )
//}