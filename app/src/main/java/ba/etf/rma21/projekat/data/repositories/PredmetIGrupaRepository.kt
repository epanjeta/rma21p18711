package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import android.util.Log
import androidx.compose.runtime.currentCompositeKeyHash
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.http.Path

class PredmetIGrupaRepository {

    companion object {

        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }

        suspend fun getPredmeti(): List<Predmet> {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getAllPredmets()
                return@withContext response
            }
        }

        suspend fun getUpisaniPredmeti() : List<Predmet>{
            return withContext(Dispatchers.IO){
                if(DBRepository.updateNow() == false){
                    var db = AppDatabase.getInstance(context)
                    var predmeti = db.predmetDao().getAll()
                    return@withContext predmeti
                }
                var db = AppDatabase.getInstance(context)
                var predmeti = db.predmetDao().getAll()
                return@withContext predmeti
            }
        }
        suspend fun getUpisaniPredmetiWS() : List<Predmet>{
            return withContext(Dispatchers.IO){
                var hash = AccountRepository.getHash()
                var grupe = ApiAdapter.retrofit.getGroupsForStudent(hash)
                var predmeti = ApiAdapter.retrofit.getAllPredmets()
                var predmetiPovrat = mutableListOf<Predmet>()
                for(predmet in predmeti){
                    var grupeUPredmetu = ApiAdapter.retrofit.getGroupsForPredmet(predmet.id)
                    for(grupa in grupeUPredmetu){
                        if(grupe.contains(grupa) && !predmetiPovrat.contains(predmet)) predmetiPovrat.add(predmet)
                    }
                }
                return@withContext predmetiPovrat.toList()
            }
        }

        suspend fun getGrupe(): List<Grupa> {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getAllGroups()
                return@withContext response
            }
        }

        suspend fun getGrupeZaPredmet(idPredmeta: Int): List<Grupa> {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getGroupsForPredmet(idPredmeta)
                return@withContext response
            }
        }

        suspend fun getGroupsZaKviz(id: Int): List<Grupa>{
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getGroupsForKviz(id)
                return@withContext response
            }
        }

        suspend fun upisiUGrupu(idGrupa: Int): Boolean {
            return withContext(Dispatchers.IO) {
                var hash = AccountRepository.getHash()
                var response = ApiAdapter.retrofit.setGroupForStudent(idGrupa, hash)
                var grupe = ApiAdapter.retrofit.getGroupsForStudent(hash)
                for(grupa in grupe){
                    if(grupa.id == idGrupa) return@withContext true
                }
                return@withContext false
            }
        }

        suspend fun getUpisaneGrupe(): List<Grupa> {
            return withContext(Dispatchers.IO) {
                if(DBRepository.updateNow() == false){
                    var db = AppDatabase.getInstance(context)
                    var grupe = db.grupaDao().getAll()
                    return@withContext grupe
                }
                var db = AppDatabase.getInstance(context)
                var grupe = db.grupaDao().getAll()
                return@withContext grupe
            }
        }
        suspend fun getUpisaneGrupeWS(): List<Grupa> {
            return withContext(Dispatchers.IO) {
                var hash = AccountRepository.getHash()
                var response = ApiAdapter.retrofit.getGroupsForStudent(hash)
                for(grupa in response){
                    grupa.nazivPredmeta = ""
                }
                return@withContext response
            }
        }

    }
}