package ba.etf.rma21.projekat

import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import ba.etf.rma21.projekat.data.repositories.AccountRepository
import ba.etf.rma21.projekat.view.FragmentKvizovi
import ba.etf.rma21.projekat.view.FragmentPredmeti
import ba.etf.rma21.projekat.viewmodel.AccountViewModel
import ba.etf.rma21.projekat.viewmodel.SharedViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.internal.ContextUtils.getActivity


class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener{

//    private lateinit var kvizoviAdapter: KvizListAdapter
//    private lateinit var kvizovi: RecyclerView
//    private var kvizListViewModel = KvizListViewModel()
//    lateinit var filterKvizova : Spinner
//    lateinit var spinnerArrayAdapter: ArrayAdapter<CharSequence>
//    lateinit var upisDugme : FloatingActionButton
//
//    var godinaZaPamtiti :Int = 1

    private lateinit var bottomNavigation: BottomNavigationView
    private val filter = IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")
    val bundle = Bundle()
    lateinit var fragmentManager: FragmentManager
    private val sharedViewModel = SharedViewModel()
    private val accountViewModel = AccountViewModel()
    //private val viewModel: SharedViewModel by activityViewModels()

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        //Toast.makeText(this, "text", Toast.LENGTH_SHORT).show()
        when (item.itemId) {

            R.id.kvizovi -> {
                val kvizoviFragment = FragmentKvizovi.newInstance()
                openFragment(kvizoviFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.predmeti -> {
                //Toast.makeText(this, "text", Toast.LENGTH_SHORT).show()
                //
                val predmetiFragment = FragmentPredmeti.newInstance()
                predmetiFragment.arguments = bundle
                openFragment(predmetiFragment)
                return@OnNavigationItemSelectedListener true
            }
//            R.id.navigation_search -> {
//                val searchFragment = SearchFragment.newInstance(" ")
//                openFragment(searchFragment)
//                return@OnNavigationItemSelectedListener true
//            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?){
        sharedViewModel.postaviKontekste(applicationContext)

        bundle.putString("godina", "default")
        bundle.putString("predmet", "default")
        bundle.putString("grupa", "default")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.novi_activity_main)

        val uri = intent
        if (uri != null) {
            val hash = uri.getStringExtra("payload")
            if (hash != null) {
                this?.let { accountViewModel.postaviHash(hash, onSuccess = ::onSuccess, onError = ::onError)}
            }
        }
        else {
            this?.let { accountViewModel.postaviHash(AccountRepository.getHash(), onSuccess = ::onSuccess, onError = ::onError)}
        }

        //accountViewModel.postaviHash(onSuccess = ::onSuccess, onError = ::onError, payload = payload)

        bottomNavigation= findViewById(R.id.bottomNav)
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        bottomNavigation.selectedItemId= R.id.kvizovi
        //val model = ViewModelProvider(requireActivity()).get(SharedViewModel::class.java)
//        val model = ViewModelProvider(this, SharedViewModel::class.java)
        bottomNavigation.menu.findItem(R.id.predajKviz).isVisible = false
        bottomNavigation.menu.findItem(R.id.zaustaviKviz).isVisible = false
        val model = ViewModelProvider(this).get(SharedViewModel::class.java)
//        model.opcijeNavigacije.observe(this, Observer<String> {
//            Toast.makeText(this, "text", Toast.LENGTH_SHORT).show()
//            bottomNavigation.menu.findItem(R.id.predmeti).isVisible = false
//            if(it == "Druge"){
//                bottomNavigation.menu.findItem(R.id.kvizovi).isVisible = false
//                bottomNavigation.menu.findItem(R.id.predmeti).isVisible = false
//                bottomNavigation.menu.findItem(R.id.predajKviz).isVisible = true
//                bottomNavigation.menu.findItem(R.id.zaustaviKviz).isVisible = true
//            }
//            else{
//                bottomNavigation.menu.findItem(R.id.kvizovi).isVisible = true
//                bottomNavigation.menu.findItem(R.id.predmeti).isVisible = true
//                bottomNavigation.menu.findItem(R.id.predajKviz).isVisible = false
//                bottomNavigation.menu.findItem(R.id.zaustaviKviz).isVisible = false
//
//                //bottomNavigation.menu.findItem(R.id.zaustaviKviz).setVisible(true)
//            }
//        })
        val kvizoviFragment = FragmentKvizovi.newInstance()
        openFragment(kvizoviFragment)



        //bottomNavigation.menu.findItem(R.id.predmeti).isVisible = false

//        if(intent?.action == Intent.ACTION_SEND && intent?.type == "text/plain")
//            handleSendText(intent)
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }



    override fun onResume() {
        super.onResume()
        //registerReceiver(br, filter)
    }

    override fun onPause() {
        //unregisterReceiver(br)
        super.onPause()
    }

    override fun onBackPressed() {

        super.onBackPressed()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, FragmentKvizovi.newInstance())
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun onSuccess(){

    }
    fun onError(){

    }

    fun getBottomNavigationView() : BottomNavigationView{
        return bottomNavigation
    }

    fun getSharedViewModel() : SharedViewModel{
        return sharedViewModel
    }

//    fun getContainer() :
//
////    private fun handleSendText(intent: Intent) {
////        intent.getStringExtra(Intent.EXTRA_TEXT)?.let {
////            bottomNavigation.selectedItemId= R.id.navigation_search
////            val searchFragment = SearchFragment.newInstance(it)
////            openFragment(searchFragment)
////        }
////    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        TODO("Not yet implemented")
    }

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
//
//        upisDugme = findViewById(R.id.upisDugme)
//        kvizovi = findViewById(R.id.listaKvizova)
//        //kvizovi.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
//        //kvizovi.layoutManager = GridLayoutManager(this, 5, GridLayoutManager.HORIZONTAL, true)
//        kvizovi.layoutManager = GridLayoutManager(this, 2)
//        kvizoviAdapter = KvizListAdapter(listOf())
//        kvizovi.adapter = kvizoviAdapter
//        kvizoviAdapter.updateKvizovi(kvizListViewModel.getKvizovi())
//
//        filterKvizova = findViewById(R.id.filterKvizova)
//        spinnerArrayAdapter = ArrayAdapter.createFromResource(this, R.array.filterKvizova, android.R.layout.simple_spinner_item)
//        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        filterKvizova.setAdapter(spinnerArrayAdapter)
//        filterKvizova.setOnItemSelectedListener(this);
//
//        upisDugme.setOnClickListener(object: View.OnClickListener {
//            override fun onClick(view: View): Unit {
//                openUpisPredmet();
//            }
//        })
//
//    }

//    override fun onNothingSelected(parent: AdapterView<*>?) {
//
//    }
//
//    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//        val text = parent!!.getItemAtPosition(position).toString()
//        //Toast.makeText(parent!!.context, text, Toast.LENGTH_SHORT).show()
//        if(text == "Urađeni kvizovi"){
//            kvizoviAdapter.updateKvizovi(kvizListViewModel.getUradjeniKvizovi())
//            kvizoviAdapter.notifyDataSetChanged()
//
//            //Toast.makeText(parent!!.context, kvizListViewModel.getUradjeniKvizovi().size.toString(), Toast.LENGTH_SHORT).show()
//        }
//        else if(text == "Prošli kvizovi"){
//            kvizoviAdapter.updateKvizovi(kvizListViewModel.getProsliKvizovi())
//            kvizoviAdapter.notifyDataSetChanged()
//        }
//        else if(text == "Budući kvizovi"){
//            kvizoviAdapter.updateKvizovi(kvizListViewModel.getBuduciKvizovi())
//            kvizoviAdapter.notifyDataSetChanged()
//        }
//        else if(text == "Svi moji kvizovi"){
//            kvizoviAdapter.updateKvizovi(kvizListViewModel.getMojiKvizovi())
//            kvizoviAdapter.notifyDataSetChanged()
//        }
//        else{
//            kvizoviAdapter.updateKvizovi(kvizListViewModel.getKvizovi())
//            kvizoviAdapter.notifyDataSetChanged()
//        }
//    }
//
//    private fun openUpisPredmet() {
////        val intent = Intent(this, UpisPredmet::class.java)
////        startActivity(intent)
//        val i = Intent(this, UpisPredmet::class.java)
//        i.putExtra("godinaPamtiti", godinaZaPamtiti)
//        startActivityForResult(i, 1)
//    }
//
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        when (requestCode) {
//            1 -> {
//                if (resultCode == Activity.RESULT_OK) {
//                    godinaZaPamtiti= data!!.getStringExtra("godina_za_postaviti")!!.toInt();
//                    if(filterKvizova.getSelectedItem().toString() == "Svi moji kvizovi"){
//                        kvizoviAdapter.updateKvizovi(kvizListViewModel.getKvizovi())
//                        kvizoviAdapter.notifyDataSetChanged()
//                        kvizoviAdapter.updateKvizovi(kvizListViewModel.getMojiKvizovi())
//                        kvizoviAdapter.notifyDataSetChanged()
//                    }
//                }
//            }
//        }
//    }
}



