//package ba.etf.rma21.projekat.data.staticdata
//
//import ba.etf.rma21.projekat.data.models.Predmet
//
//fun sviPredmeti() : List<Predmet>{
//    return listOf(
//        Predmet("NSI", 5),
//        Predmet("MPVI", 5),
//        Predmet("RI", 5),
//        Predmet("NASP", 4),
//        Predmet("MU", 4),
//        Predmet("BP", 4),
//        Predmet("OPI", 3),
//        Predmet("RMS", 3),
//        Predmet("PJP", 3),
//        Predmet("RPR", 2),
//        Predmet("DM", 2),
//        Predmet("RMA", 2),
//        Predmet("IM", 1),
//        Predmet("LAG", 1),
//        Predmet("TP", 1)
//    )
//}