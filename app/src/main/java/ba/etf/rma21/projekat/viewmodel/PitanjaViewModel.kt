package ba.etf.rma21.projekat.viewmodel

import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Odgovor
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.data.repositories.OdgovorRepository
import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository
import ba.etf.rma21.projekat.data.repositories.TakeKvizRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.reflect.KFunction1

class PitanjaViewModel {
    val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun getPitanja(onSuccess: (pitanja: List<Pitanje>)-> Unit,
                           onError: () -> Unit, idKviza: Int) {
        scope.launch {
            val result = KvizRepository.getById(idKviza)
            when (result) {
                is Kviz -> {
                    val result2 = PitanjeKvizRepository.getPitanja(result.id)
                    onSuccess?.invoke(result2)
                }
                else-> onError?.invoke()
            }
        }
    }

    fun dajOpcije(pitanje: Pitanje) : List<String>{
        return PitanjeKvizRepository.dajOpcije(pitanje)
    }

     fun postaviUradjeno(onSuccess: (int: Int) -> Unit,
                                onError: () -> Unit, kvizTakenId:Int, pitanje: Pitanje, brojOdgovora: Int){
        scope.launch {
            val result = OdgovorRepository.postaviOdgovorKviz(kvizTakenId, pitanje.id, brojOdgovora)
            onSuccess?.invoke(result)
        }
    }

//    fun dajOdgovore(onSuccess: (odgovori: List<Odgovor>) -> Unit,
//                    onError: () -> Unit, idKviza: Int){
//        scope.launch {
//            var result = OdgovorRepository.getOdgovoriKviz(idKviza)
//            onSuccess?.invoke(result)
//        }
//    }
    fun dajOdgovore(onSuccess: (odgovori: List<Odgovor>) -> Unit,
                    onError: () -> Unit, idKviza: Int){
        scope.launch {
            var result = OdgovorRepository.dajOdgovore(idKviza)
            onSuccess?.invoke(result)
        }
    }

    fun getBodoviZaKviz(onSuccess: (bodovi: Int) -> Unit,
                        onError: () -> Unit, idPokusaja: Int){
        scope.launch {
            val result = TakeKvizRepository.getBodoviZaKviz(idPokusaja)
            onSuccess?.invoke(result)
        }
    }

    fun predajOdgovore(onSuccess: () -> Unit, onError: () -> Unit, idKviza: Int){
        scope.launch {
            OdgovorRepository.predajOdgovore(idKviza)
        }
    }
}