package ba.etf.rma21.projekat.data.models

data class PitanjeKviz (
        val naziv:String,
        val kviz:String,
        var odgovoren:Int
)