package ba.etf.rma21.projekat.data.models

import org.json.JSONObject
import retrofit2.http.*

interface Api {
    @GET("/kviz")
    suspend fun getAllKvizes(): List<Kviz>

    @GET("/kviz/{id}")
    suspend fun getKvizById(@Path("id") id: Int): Kviz

    @GET("/grupa/{id}/kvizovi")
    suspend fun getKvizesForGroup(@Path("id") id: Int): List<Kviz>


    @GET("/predmet")
    suspend fun getAllPredmets() : List<Predmet>

    @GET("/predmet/{id}")
    suspend fun getPredmetById(@Path("id") id: Int): Predmet


    @GET("/kviz/{id}/grupa")
    suspend fun getGroupsForKviz(@Path("id") id: Int): List<Grupa>

    @POST("/grupa/{gid}/student/{id}")
    suspend fun setGroupForStudent(@Path("gid") gid: Int, @Path("id") id: String)

    @GET("/student/{id}/grupa")
    suspend fun getGroupsForStudent(@Path("id") id: String): List<Grupa>

    @GET("/grupa")
    suspend fun getAllGroups() : List<Grupa>

    @GET("/grupa/{id}")
    suspend fun getGroupById(@Path("id") id: Int): Grupa

    @GET("/predmet/{id}/grupa")
    suspend fun getGroupsForPredmet(@Path("id") id: Int): List<Grupa>


    @GET("/student/{id}")
    suspend fun getAccountForHash(@Path("id") id: String): Account


    @GET("/student/{id}/kviztaken/{ktid}/odgovori")
    suspend fun getAnswersForAttempt(@Path("id") id: String, @Path("ktid") ktid: Int) : List<Odgovor>

    @POST("/student/{id}/kviztaken/{ktid}/odgovor")
    suspend fun setAnswerForAttempt(@Path("id") id: String, @Path("ktid") ktid: Int, @Body odgovorBody: OdgovorBody)


    @GET("/student/{id}/kviztaken")
    suspend fun getAttempts(@Path("id") id: String) : List<KvizTaken>

    @POST("/student/{id}/kviz/{kid}")
    suspend fun startAttempt(@Path("id") id: String, @Path("kid") kid: Int)

    @GET("/kviz/{id}/pitanja")
    suspend fun getQuestionsForKviz(@Path("id") id: Int) : List<Pitanje>

    @GET("/account/{id}/lastUpdate")
    suspend fun updateNow(@Path("id") id: String, @Query("date") stringDate: String) : UpdateResponse
}