package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Odgovor

@Dao
interface OdgovorDao {

    @Query("SELECT * FROM Odgovor")
    suspend fun getAll() : List<Odgovor>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg odgovori: Odgovor)

    @Query("DELETE FROM Odgovor")
    suspend fun deleteAll()


}