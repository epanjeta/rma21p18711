package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.repositories.GrupaRepository

class FragmentPoruka : Fragment() {
    private lateinit var text: TextView
    private lateinit var predmet: String
    private lateinit var grupat: String
    private lateinit var grupa: Grupa
    private lateinit var nazivKviza: String
    private lateinit var procentTacnost: String
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view =  inflater.inflate(R.layout.poruka_fragment, container, false)
        text = view.findViewById(R.id.tvPoruka)
        if(arguments?.getString("predmet") == "da") {
            predmet = arguments?.getString("predmetNaziv").toString()
            grupat = arguments?.getString("grupa").toString()
            //grupa = GrupaRepository.dajGrupuZaPredmet(predmet)!!
            text.setText("Uspješno ste upisani u grupu " + grupat + " predmeta " + predmet);
        }
        else{
            nazivKviza = arguments?.getString("nazivKviza").toString()
            procentTacnost = arguments?.getString("procenatTacnosti").toString()
            text.setText("Završili ste kviz " + nazivKviza + " sa tačnosti " + procentTacnost);
        }
        return view
    }
    companion object {
        fun newInstance(): FragmentPoruka = FragmentPoruka()
    }
}