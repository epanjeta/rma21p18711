package ba.etf.rma21.projekat.data.models

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Grupa(
    @PrimaryKey @SerializedName("id") var id: Int,
    @ColumnInfo(name = "naziv") @SerializedName("naziv") var naziv: String?,
    var nazivPredmeta: String
) {
    override fun toString(): String {
        if(naziv.isNullOrEmpty()) return "";
        else return naziv.toString()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Grupa

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id
    }
}

//data class Grupa(val naziv: String, val nazivPredmeta: String) {
//    override fun toString(): String {
//        return naziv
//    }
//}