package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import ba.etf.rma21.projekat.MainActivity
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.viewmodel.UpisPredmetaViewModel


class FragmentPredmeti: Fragment(), AdapterView.OnItemSelectedListener {



    private var upisPredmet = UpisPredmetaViewModel()

    private lateinit var spinnerGodina : Spinner
    lateinit var spinnerGodinaArrayAdapter : ArrayAdapter<CharSequence>

    private lateinit var spinnerPredmeti : Spinner
    lateinit var spinnerPredmetiArrayAdapter : ArrayAdapter<Predmet>

    private lateinit var spinnerGrupa : Spinner
    lateinit var spinnerGrupaArrayAdapter : ArrayAdapter<Grupa>

    private lateinit var buttonDodaj : Button

    private  var listaPredmeta: List<Predmet> = listOf()
    private var listaGrupa: List<Grupa> = listOf()


    var bundle = Bundle()
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        var view =  inflater.inflate(R.layout.activity_upis_predmet, container, false)
        spinnerGodina = view.findViewById(R.id.odabirGodina)
        spinnerPredmeti = view.findViewById(R.id.odabirPredmet)
        spinnerGrupa = view.findViewById(R.id.odabirGrupa)

        buttonDodaj = view.findViewById(R.id.dodajPredmetDugme)

        spinnerGodinaArrayAdapter = this.context?.let { ArrayAdapter.createFromResource(it, R.array.godine, android.R.layout.simple_spinner_item) } as ArrayAdapter<CharSequence>
        spinnerGodinaArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerGodina.setAdapter(spinnerGodinaArrayAdapter)
        spinnerGodina.setOnItemSelectedListener(this)




        buttonDodaj.isEnabled = false
        buttonDodaj.isClickable = false


        buttonDodaj.setOnClickListener(object: View.OnClickListener {
            override fun onClick(view: View): Unit {
                upisiPredmet()
                bundle.putString("godina", spinnerGodina.getSelectedItem().toString())
                bundle.putString("predmetNaziv", spinnerPredmeti.getSelectedItem().toString())
                bundle.putString("grupa", spinnerGrupa.getSelectedItem().toString())
                bundle.putString("upisano", "Da")
                bundle.putString("kviz", "ne")
                bundle.putString("predmet", "da")
                val porukaFragment = FragmentPoruka.newInstance()
                porukaFragment.arguments = bundle
                val fragmentManager: FragmentManager? = getFragmentManager()
                if (fragmentManager != null) {
                    fragmentManager.beginTransaction().replace(R.id.container, porukaFragment).commit()
                }

            }
        })


        var listaPredmeta1:Array<String> = dajNazivePredmeta(listaPredmeta)
        listaPredmeta1 = dodajPrazanString(listaPredmeta1)
        var adapter1: ArrayAdapter<String>? = this.context?.let {
            ArrayAdapter(it,
                    android.R.layout.simple_spinner_item, listaPredmeta1)
        }
        if (adapter1 != null) {
            adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        spinnerPredmeti.setAdapter(adapter1)
        spinnerPredmeti.setOnItemSelectedListener(this)

        var listaGrupa1:Array<String> = dajNaziveGrupa(listaGrupa)
        listaGrupa1 = dodajPrazanString(listaGrupa1)
        var adapter: ArrayAdapter<String>? = this.context?.let {
            ArrayAdapter(it,
                    android.R.layout.simple_spinner_item, listaGrupa1)
        }
        if (adapter != null) {
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        spinnerGrupa.setAdapter(adapter)
        spinnerGrupa.setOnItemSelectedListener(this)

        return view
    }

    companion object {
        fun newInstance(): FragmentPredmeti = FragmentPredmeti()
    }


    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        var aktivnostMain = activity as MainActivity
        if (parent != null) {
            if(parent.id == R.id.odabirGodina){
                val text = parent!!.getItemAtPosition(position).toString()
                val godina = text.toInt()
                //var listaPredmeta:Array<String> = dajNazivePredmeta(upisPredmet.getPredmetsGodinaNeUpisani(godina))

                upisPredmet.getPredmetiZaGodinu(onSuccess = ::onSuccessPredmeti, onError = ::onError, godina = godina)
//                val toast = Toast.makeText(context, godina.toString(), Toast.LENGTH_SHORT)
//                toast.show()
            }
            else if(parent.id == R.id.odabirPredmet){

                val text = parent!!.getItemAtPosition(position).toString()
                upisPredmet.getGrupePredmet(onSuccess = ::onSuccessGrupe, onError = ::onError, nazivPredmeta = text)

//                var listaGrupa1:Array<String> = dajNaziveGrupa(upisPredmet.getGrupePredmet(text))
//                listaGrupa1 = dodajPrazanString(listaGrupa1)
//                var adapter: ArrayAdapter<String>? = this.context?.let {
//                    ArrayAdapter(it,
//                            android.R.layout.simple_spinner_item, listaGrupa1)
//                }
//                if (adapter != null) {
//                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//                }
//                spinnerGrupa.setAdapter(adapter)
//                spinnerGrupa.setOnItemSelectedListener(this)
            }
            else if(parent.id == R.id.odabirGrupa){
            }
            if(spinnerPredmeti.getSelectedItem()!="" && spinnerGrupa.getSelectedItem()!=""){
                buttonDodaj.isEnabled = true
                buttonDodaj.isClickable = true
            }
        }
    }
    private fun upisiPredmet(){

        if(dajGrupuZaNaziv(spinnerGrupa.getSelectedItem().toString())!=null){
            dajGrupuZaNaziv(spinnerGrupa.getSelectedItem().toString())?.let { upisPredmet.upisiGrupu(onSuccess = ::onSuccessUpis, onError = ::onError, grupa = it) }
        }

        //upisPredmet.dodajPredmetUUpisane(spinnerPredmeti.getSelectedItem().toString(), spinnerGrupa.getSelectedItem().toString())

    }
    private fun dajNazivePredmeta(lista: List<Predmet>) : Array<String>{
        val povrat: MutableList<String> = mutableListOf()
        for(predmet in lista){
            predmet.naziv?.let { povrat.add(it) }
        }
        return povrat.toTypedArray()
    }
    private fun dajNaziveGrupa(lista : List<Grupa>) : Array<String>{
        val povrat: MutableList<String> = mutableListOf()
        for(grupa in lista){
            grupa.naziv?.let { povrat.add(it) }
        }
        return povrat.toTypedArray()
    }
    private fun dodajPrazanString(pocetniString : Array<String>): Array<String>{
        var noviString : Array<String> = arrayOf(" ")
        noviString.set(0, "")
        var lista = noviString.toMutableList()
        for(element in pocetniString){
            lista.add(element)
        }
        return lista.toTypedArray()
    }
    fun onSuccessPredmeti(noviPredmeti: List<Predmet>){
        listaPredmeta = noviPredmeti
        var listaPredmeta1:Array<String> = dajNazivePredmeta(listaPredmeta)
        listaPredmeta1 = dodajPrazanString(listaPredmeta1)
        var adapter: ArrayAdapter<String>? = this.context?.let {
            ArrayAdapter(it,
                    android.R.layout.simple_spinner_item, listaPredmeta1)
        }
        if (adapter != null) {
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        spinnerPredmeti.setAdapter(adapter)
        spinnerPredmeti.setOnItemSelectedListener(this)

    }
    fun onSuccessGrupe(noveGrupe:List<Grupa>){
        listaGrupa = noveGrupe
        var listaGrupa1:Array<String> = dajNaziveGrupa(listaGrupa)
        listaGrupa1 = dodajPrazanString(listaGrupa1)
        var adapter: ArrayAdapter<String>? = this.context?.let {
            ArrayAdapter(it,
                    android.R.layout.simple_spinner_item, listaGrupa1)
        }
        if (adapter != null) {
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        spinnerGrupa.setAdapter(adapter)
        spinnerGrupa.setOnItemSelectedListener(this)
    }
    fun onSuccessUpis(){

    }
    fun onError() {
        val toast = Toast.makeText(context, "error", Toast.LENGTH_SHORT)
        toast.show()
    }
    fun dajGrupuZaNaziv(naziv: String) : Grupa? {
        for(grupa in listaGrupa){
            if(grupa.naziv == naziv) return grupa
        }
        return null
    }
}