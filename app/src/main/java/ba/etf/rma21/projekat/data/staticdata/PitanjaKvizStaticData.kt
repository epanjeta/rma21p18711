package ba.etf.rma21.projekat.data.staticdata

import ba.etf.rma21.projekat.data.models.PitanjeKviz

fun getPitanjeKviz() : List<PitanjeKviz>{
    return listOf(

            //RMA - OTVOREN
            PitanjeKviz("Pitanje1", "Kviz2", -1),
            PitanjeKviz("Pitanje2", "Kviz2", -1),
            PitanjeKviz("Pitanje3", "Kviz2", -1),

            //DM - ČEKA SE
            PitanjeKviz("Pitanje4", "Kviz1", -1),
            PitanjeKviz("Pitanje5", "Kviz1", -1),

            //IM - URADJEN
            PitanjeKviz("Pitanje6", "Kviz3", 1),
            PitanjeKviz("Pitanje7", "Kviz3", 2),

            //RPR - ISTEKAO ROK
            PitanjeKviz("Pitanje8", "Kviz8", -1),
            PitanjeKviz("Pitanje9", "Kviz8", -1)


    )
}
