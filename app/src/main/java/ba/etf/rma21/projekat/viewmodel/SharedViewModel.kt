package ba.etf.rma21.projekat.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ba.etf.rma21.projekat.data.repositories.*

class SharedViewModel : ViewModel(){

    var bodovi : Int = 0
    val boja =  MutableLiveData<String>()
    fun postaviZelenu(){
        boja.value = "#3DDC84"
    }
    fun postaviCrvenu(){
        boja.value = "#DB4F3D"
    }
    fun postaviBodove(bodovi:Int){
        this.bodovi = bodovi
    }
    var indexGodine = MutableLiveData<Int>(0)
    var indexPredmeta = MutableLiveData<Int>(0)
    var indexGrupe = MutableLiveData<Int>(0)


    fun postaviKontekste(context: Context){
        AccountRepository.setContext(context)
        KvizRepository.setContext(context)
        OdgovorRepository.setContext(context)
        PitanjeKvizRepository.setContext(context)
        PredmetIGrupaRepository.setContext(context)
        TakeKvizRepository.setContext(context)
        DBRepository.setContext(context)
    }

}