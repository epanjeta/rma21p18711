package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.models.Odgovor

@Dao
interface KvizTakenDao {

    @Query("SELECT * FROM KvizTaken")
    suspend fun getAll() : List<KvizTaken>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg kvizTaken: KvizTaken)

    @Query("DELETE FROM KvizTaken")
    suspend fun deleteAll()
}