package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import android.widget.Toast
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.viewmodel.PitanjaViewModel
//import ba.etf.rma21.projekat.data.staticdata.sviKvizovi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

class KvizRepository {

    companion object {

        private lateinit var context:Context
        private var upravoPredan = false
        fun setContext(_context:Context){
            context=_context
        }

        init {
        }

        suspend fun insertAll(context: Context, kviz: Kviz) : String?{
            return withContext(Dispatchers.IO){
                try{
                    var db = AppDatabase.getInstance(context)
                    db.kvizDao().insertAll(kviz)
                    return@withContext "dobar"
                }catch (error: Exception) {
                    return@withContext null
                }
            }
        }

        suspend fun getAll(context: Context) : List<Kviz>{
            return withContext(Dispatchers.IO){
                var db = AppDatabase.getInstance(context)
                var kvizovi = db.kvizDao().getAll()
                return@withContext kvizovi
            }
        }

        suspend fun getAll() : List<Kviz> {
           return withContext(Dispatchers.IO){
               var response = ApiAdapter.retrofit.getAllKvizes()
               for(kviz in response){
                   popuniPodatkeZaKviz(kviz)
               }
//               for(kviz in response){
//                   insertAll(applicationContext, )
//               }
               return@withContext response
           }
        }
        suspend fun getDone() : List<Kviz> {
            return withContext(Dispatchers.IO){
                var response = ApiAdapter.retrofit.getAllKvizes()
                response = getUpisani()
//                for(kviz in response){
//                    popuniPodatkeZaKviz(kviz)
//                }
                var lista = response.toMutableList()
                var listaPovrat = mutableListOf<Kviz>()
                for(kviz in lista){
                    if(kviz.datumRada != null) listaPovrat.add(kviz)
                }
                return@withContext listaPovrat.toList()
            }
        }
        suspend fun getFuture() : List<Kviz> {
            return withContext(Dispatchers.IO){
                var response = ApiAdapter.retrofit.getAllKvizes()
                response = getUpisani()
//                for(kviz in response){
//                    popuniPodatkeZaKviz(kviz)
//                }
                val c = Calendar.getInstance()
                var lista = response.toMutableList()
                var listaPovrat = mutableListOf<Kviz>()
                for(kviz in lista){
                if(kviz.datumRada == null && kviz.datumPocetka > c.time){
                    listaPovrat.add(kviz)
                }
            }
                return@withContext listaPovrat.toList()
            }
        }
        suspend fun getNotTaken() : List<Kviz> {
            return withContext(Dispatchers.IO){
                var response = ApiAdapter.retrofit.getAllKvizes()
                response = getUpisani()
//                for(kviz in response){
//                    popuniPodatkeZaKviz(kviz)
//                }
                var lista = response.toMutableList()
                val c = Calendar.getInstance()
                var listaPovrat = mutableListOf<Kviz>()
                for(kviz in lista){
                    if(kviz.datumKraj!=null &&  kviz.datumRada == null && kviz.datumKraj!! < c.time && kviz.osvojeniBodovi == null){
                        listaPovrat.add(kviz)
                    }
                }
                return@withContext listaPovrat.toList()
            }
        }

        suspend fun getMyKvizes() : List<Kviz> {
            return withContext(Dispatchers.IO){
                var response = ApiAdapter.retrofit.getAllKvizes()
                for(kviz in response){
                    popuniPodatkeZaKviz(kviz)
                }
                return@withContext response
            }
        }

        suspend fun getById(id:Int):Kviz? {
            return withContext(Dispatchers.IO){
                var response = ApiAdapter.retrofit.getAllKvizes()
                for(kviz in response){
                    if(kviz.id == id) return@withContext kviz
                }
                return@withContext null
            }
        }

        suspend fun popuniPodatkeZaKviz(kviz: Kviz): Kviz{
            var brojPredmeta:Int = 0
            var predmeti = mutableListOf<Predmet>()
            var listaGrupaZaKviz = PredmetIGrupaRepository.getGroupsZaKviz(kviz.id)
            var listaPredmeta = PredmetIGrupaRepository.getPredmeti()
            for(predmet in listaPredmeta){
                var listaGrupaUPredmetu = PredmetIGrupaRepository.getGrupeZaPredmet(predmet.id)
                for(grupa in listaGrupaUPredmetu){
                    if(listaGrupaZaKviz.contains(grupa) && !predmeti.contains(predmet)){
                        if(brojPredmeta == 0) {
                            kviz.nazivPredmeta = predmet.naziv.toString()
                            brojPredmeta++
                            predmeti.add(predmet)
                        }
                        else{
                            kviz.nazivPredmeta = kviz.nazivPredmeta + " " + predmet.naziv.toString()
                            brojPredmeta++
                            predmeti.add(predmet)
                        }
                    }
                }
            }
            kviz.osvojeniBodovi = null
            var listaPokusaja = TakeKvizRepository.getPocetiKvizovi()
            if (listaPokusaja != null) {
                for(pokusaj in listaPokusaja){
                    if(pokusaj.KvizId == kviz.id){
                        kviz.osvojeniBodovi = pokusaj.osvojeniBodovi.toFloat()
                    }
                }
            }
            var pitanja = PitanjeKvizRepository.getPitanjaWS(kviz.id)
            var odgovori = OdgovorRepository.getOdgovoriKviz(kviz.id)
            var pokusaji = TakeKvizRepository.getPocetiKvizovi()
            if(odgovori.size == pitanja.size){
                kviz.predan = true
                if (pokusaji != null) {
                    for(pokusaj in pokusaji){
                        if(pokusaj.KvizId == kviz.id) kviz.datumRada = pokusaj.datumRada
                    }
                }
            }
            else kviz.predan = false
            kviz.nazivGrupe = ""
            return kviz
        }

        suspend fun getUpisani():List<Kviz>{

//            val toast = Toast.makeText(context, "Ovo će malo potrajati", Toast.LENGTH_SHORT)
//            toast.show()

            if(DBRepository.updateNow() == false){

                var kvizovi = getAll(this.context)

                return kvizovi
            }

            var kvizovi = getAll(this.context)

            return kvizovi

//            var sviPredmeti = PredmetIGrupaRepository.getPredmeti()
//            var upisaneGrupe = PredmetIGrupaRepository.getUpisaneGrupe()
//            var upisaniPredemti = mutableListOf<Predmet>()
//            for(predmet in sviPredmeti){
//                var grupeNaPredmetu = PredmetIGrupaRepository.getGrupeZaPredmet(predmet.id)
//                for(grupaNaPredmetu in grupeNaPredmetu){
//                    if(upisaneGrupe.contains(grupaNaPredmetu)) upisaniPredemti.add(predmet)
//                }
//            }
//            var upisaniKvizovi = mutableListOf<Kviz>()
//            var sviKvizovi = getAll()
//            for(predmet in upisaniPredemti) {
//                for (kviz in sviKvizovi) {
//
//                    if (predmet.naziv?.let { kviz.nazivPredmeta.contains(it) }!!) upisaniKvizovi.add(kviz)
//                }
//            }
//            return upisaniKvizovi
        }

        suspend fun getUpisaniWS() : List<Kviz>{
            var sviPredmeti = PredmetIGrupaRepository.getPredmeti()
            var upisaneGrupe = PredmetIGrupaRepository.getUpisaneGrupeWS()
            var upisaniPredemti = mutableListOf<Predmet>()
            for(predmet in sviPredmeti){
                var grupeNaPredmetu = PredmetIGrupaRepository.getGrupeZaPredmet(predmet.id)
                for(grupaNaPredmetu in grupeNaPredmetu){
                    if(upisaneGrupe.contains(grupaNaPredmetu)) upisaniPredemti.add(predmet)
                }
            }
            var upisaniKvizovi = mutableListOf<Kviz>()
            var sviKvizovi = getAll()
            for(predmet in upisaniPredemti) {
                for (kviz in sviKvizovi) {

                    if (predmet.naziv?.let { kviz.nazivPredmeta.contains(it) }!!) upisaniKvizovi.add(kviz)
                }
            }
            return upisaniKvizovi
        }

        fun postaviUradjen(idKviza: Int, bodovi: Float) {

        }

        fun dajUradjen(nazivKviza: String): Boolean {
            return false
        }

        fun dajStatusKviza(kviz: Kviz): String {
            if(kviz.datumPocetka > Calendar.getInstance().time) return "Budući"
            if(kviz.datumKraj != null){
                if(kviz.datumKraj!! < Calendar.getInstance().time) return "Prošao"
            }
            return ""
        }

        suspend fun dajStatusKviza(kvizId: Int) : String{
            return withContext(Dispatchers.IO){
                var kviz = getById(kvizId)
                if(kviz!=null){
                    if(kviz.datumPocetka > Calendar.getInstance().time) return@withContext "Budući"
                    if(kviz.datumKraj != null){
                        if(kviz.datumKraj!! < Calendar.getInstance().time) return@withContext "Prošao"
                    }
                    return@withContext ""
                }
                return@withContext ""
            }
        }
    }
}
