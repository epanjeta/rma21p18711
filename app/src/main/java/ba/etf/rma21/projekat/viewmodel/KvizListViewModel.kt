package ba.etf.rma21.projekat.viewmodel

import android.content.Context
import android.util.Log
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.data.repositories.TakeKvizRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.util.*

class KvizListViewModel {

    val scope = CoroutineScope(Job() + Dispatchers.Main)


    fun dajUradjen(nazivKviza: String) : Boolean{
        return KvizRepository.dajUradjen(nazivKviza)
    }
    fun dajStatusKviza(kviz: Kviz) : String{
        return KvizRepository.dajStatusKviza(kviz)
    }

    fun dajStatusKviza(onSuccess: (status: String) -> Unit,
                       onError: () -> Unit, idKviza: Int){
        scope.launch {
            var result = KvizRepository.dajStatusKviza(idKviza)
            onSuccess?.invoke(result)
        }
    }


    fun zapocniKviz( onSuccess: (kvizTaken: KvizTaken) -> Unit,
                    onError: () -> Unit, kviz: Kviz){
        scope.launch{

            val result = TakeKvizRepository.zapocniKviz(kviz.id)
            if (result != null) {

                onSuccess?.invoke(result)
            }
            else{

                onError?.invoke()
            }
        }
    }

    fun getUpisani(onSuccess: (kvizovi: List<Kviz>) -> Unit,
                   onError: () -> Unit){
        scope.launch {
            val result = KvizRepository.getUpisani()
            onSuccess?.invoke(result)
        }
    }

    fun dajKvizove( onSuccess: (movies: List<Kviz>) -> Unit,
                     onError: () -> Unit){
        scope.launch{
            val result = KvizRepository.getAll()
            onSuccess?.invoke(result)
        }
    }
    fun getUradjeniKvizovi( onSuccess: (movies: List<Kviz>) -> Unit,
                    onError: () -> Unit){
        scope.launch{
            val result = KvizRepository.getDone()
            onSuccess?.invoke(result)
        }
    }
    fun getProsliKvizovi( onSuccess: (movies: List<Kviz>) -> Unit,
                    onError: () -> Unit){
        scope.launch{
            val result = KvizRepository.getNotTaken()
            onSuccess?.invoke(result)
        }
    }
    fun getBuduciKvizovi( onSuccess: (movies: List<Kviz>) -> Unit,
                    onError: () -> Unit){
        scope.launch{
            val result = KvizRepository.getFuture()
            onSuccess?.invoke(result)
        }
    }
    fun insertAll(context: Context, kviz: Kviz, onSuccess: () -> Unit, onError: () -> Unit){
        scope.launch {
            val result = KvizRepository.insertAll(context, kviz)
            if(result != null){
                onSuccess?.invoke()
            }
            else{
                onError?.invoke()
            }
        }
    }
//    fun getDone(context: Context,  onSuccess: (kvizovi: List<Kviz>) -> Unit, onError: () -> Unit){
//        scope.launch {
//            val result = KvizRepository.getDone(context)
//
//        }
//    }
}