//package ba.etf.rma21.projekat
//
//import ba.etf.rma21.projekat.data.models.Kviz
//import ba.etf.rma21.projekat.viewmodel.UpisPredmetaViewModel
//import junit.framework.TestCase
//import org.hamcrest.CoreMatchers
//import org.hamcrest.Matchers
//import org.junit.Assert
//
//class UpisPredmetaViewModelTest : TestCase() {
//
//    val upisPredmetaViewModel = UpisPredmetaViewModel()
//
//    fun testGetPredmetsGodina1() {
//        assertEquals(3, upisPredmetaViewModel.getPredmetsGodina(1).size)
//        Assert.assertThat(upisPredmetaViewModel.getPredmetsGodina(1), Matchers.hasItem<Kviz>(Matchers.hasProperty("naziv", CoreMatchers.`is`("IM"))))
//        Assert.assertThat(upisPredmetaViewModel.getPredmetsGodina(1), Matchers.hasItem<Kviz>(Matchers.hasProperty("naziv", CoreMatchers.`is`("TP"))))
//        Assert.assertThat(upisPredmetaViewModel.getPredmetsGodina(1), Matchers.hasItem<Kviz>(Matchers.hasProperty("naziv", CoreMatchers.`is`("LAG"))))
//    }
//    fun testGetPredmetsGodina2() {
//        assertEquals(3, upisPredmetaViewModel.getPredmetsGodina(2).size)
//        Assert.assertThat(upisPredmetaViewModel.getPredmetsGodina(2), Matchers.hasItem<Kviz>(Matchers.hasProperty("naziv", CoreMatchers.`is`("RMA"))))
//        Assert.assertThat(upisPredmetaViewModel.getPredmetsGodina(2), Matchers.hasItem<Kviz>(Matchers.hasProperty("naziv", CoreMatchers.`is`("RPR"))))
//        Assert.assertThat(upisPredmetaViewModel.getPredmetsGodina(2), Matchers.hasItem<Kviz>(Matchers.hasProperty("naziv", CoreMatchers.`is`("DM"))))
//    }
//    fun testGetPredmetsGodina3() {
//        assertEquals(3, upisPredmetaViewModel.getPredmetsGodina(3).size)
//        Assert.assertThat(upisPredmetaViewModel.getPredmetsGodina(3), Matchers.hasItem<Kviz>(Matchers.hasProperty("naziv", CoreMatchers.`is`("PJP"))))
//        Assert.assertThat(upisPredmetaViewModel.getPredmetsGodina(3), Matchers.hasItem<Kviz>(Matchers.hasProperty("naziv", CoreMatchers.`is`("RMS"))))
//        Assert.assertThat(upisPredmetaViewModel.getPredmetsGodina(3), Matchers.hasItem<Kviz>(Matchers.hasProperty("naziv", CoreMatchers.`is`("OPI"))))
//    }
//    fun testGetPredmetsGodina4() {
//        assertEquals(3, upisPredmetaViewModel.getPredmetsGodina(4).size)
//        Assert.assertThat(upisPredmetaViewModel.getPredmetsGodina(4), Matchers.hasItem<Kviz>(Matchers.hasProperty("naziv", CoreMatchers.`is`("BP"))))
//        Assert.assertThat(upisPredmetaViewModel.getPredmetsGodina(4), Matchers.hasItem<Kviz>(Matchers.hasProperty("naziv", CoreMatchers.`is`("MU"))))
//        Assert.assertThat(upisPredmetaViewModel.getPredmetsGodina(4), Matchers.hasItem<Kviz>(Matchers.hasProperty("naziv", CoreMatchers.`is`("NASP"))))
//    }
//    fun testGetPredmetsGodina5() {
//        assertEquals(3, upisPredmetaViewModel.getPredmetsGodina(5).size)
//        Assert.assertThat(upisPredmetaViewModel.getPredmetsGodina(5), Matchers.hasItem<Kviz>(Matchers.hasProperty("naziv", CoreMatchers.`is`("RI"))))
//        Assert.assertThat(upisPredmetaViewModel.getPredmetsGodina(5), Matchers.hasItem<Kviz>(Matchers.hasProperty("naziv", CoreMatchers.`is`("MPVI"))))
//        Assert.assertThat(upisPredmetaViewModel.getPredmetsGodina(5), Matchers.hasItem<Kviz>(Matchers.hasProperty("naziv", CoreMatchers.`is`("NSI"))))
//    }
//
//    fun testGetGrupePredmet1() {
//        assertEquals(1, upisPredmetaViewModel.getGrupePredmet("DM").size)
//        assertEquals(2, upisPredmetaViewModel.getGrupePredmet("RMA").size)
//        Assert.assertThat(upisPredmetaViewModel.getGrupePredmet("RMA"), Matchers.hasItem<Kviz>(Matchers.hasProperty("naziv", CoreMatchers.`is`("Grupa1"))))
//        Assert.assertThat(upisPredmetaViewModel.getGrupePredmet("RMA"), Matchers.hasItem<Kviz>(Matchers.hasProperty("naziv", CoreMatchers.`is`("Grupa2"))))
//        Assert.assertThat(upisPredmetaViewModel.getGrupePredmet("DM"), Matchers.hasItem<Kviz>(Matchers.hasProperty("naziv", CoreMatchers.`is`("Grupa1"))))
//
//
//    }
//    fun testGetGrupePredmet2() {
//        assertEquals(0, upisPredmetaViewModel.getGrupePredmet("PJP").size)
//    }
//}