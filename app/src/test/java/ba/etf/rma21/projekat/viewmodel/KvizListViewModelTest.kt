//package ba.etf.rma21.projekat
//
//import ba.etf.rma21.projekat.data.models.Kviz
//import ba.etf.rma21.projekat.viewmodel.KvizListViewModel
//import junit.framework.TestCase
//import org.hamcrest.CoreMatchers
//import org.hamcrest.Matchers
//import org.junit.Assert
//
//
//
//class KvizListViewModelTest : TestCase() {
//
//    val kvizListViewModel = KvizListViewModel()
//
//    fun testGetKvizovi() {
//        assertEquals(9, kvizListViewModel.getKvizovi().size)
//    }
//
//    fun testGetUradjeniKvizovi() {
//        assertEquals(1, kvizListViewModel.getUradjeniKvizovi().size)
//        Assert.assertThat(kvizListViewModel.getUradjeniKvizovi(), Matchers.hasItem<Kviz>(Matchers.hasProperty("nazivPredmeta", CoreMatchers.`is`("IM"))))
//    }
//
//    fun testGetProsliKvizovi() {
//        assertEquals(4, kvizListViewModel.getProsliKvizovi().size)
//        Assert.assertThat(kvizListViewModel.getProsliKvizovi(), Matchers.hasItem<Kviz>(Matchers.hasProperty("nazivPredmeta", CoreMatchers.`is`("LAG"))))
//        Assert.assertThat(kvizListViewModel.getProsliKvizovi(), Matchers.hasItem<Kviz>(Matchers.hasProperty("nazivPredmeta", CoreMatchers.`is`("RPR"))))
//    }
//
//    fun testGetBuduciKvizovi() {
//        assertEquals(3, kvizListViewModel.getBuduciKvizovi().size)
//        Assert.assertThat(kvizListViewModel.getBuduciKvizovi(), Matchers.hasItem<Kviz>(Matchers.hasProperty("nazivPredmeta", CoreMatchers.`is`("DM"))))
//        Assert.assertThat(kvizListViewModel.getBuduciKvizovi(), Matchers.hasItem<Kviz>(Matchers.hasProperty("nazivPredmeta", CoreMatchers.`is`("TP"))))
//    }
//
//    fun testGetMojiKvizovi() {
//        assertEquals(2, kvizListViewModel.getMojiKvizovi().size)
//        Assert.assertThat(kvizListViewModel.getMojiKvizovi(), Matchers.hasItem<Kviz>(Matchers.hasProperty("nazivPredmeta", CoreMatchers.`is`("DM"))))
//        Assert.assertThat(kvizListViewModel.getMojiKvizovi(), Matchers.hasItem<Kviz>(Matchers.hasProperty("nazivPredmeta", CoreMatchers.`is`("RMA"))))
//
//    }
//}